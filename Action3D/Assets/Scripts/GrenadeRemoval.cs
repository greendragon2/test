using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeRemoval : MonoBehaviour
{
    public GameObject Grenade;
    void Update()
    {
        if (Grenade == null)
        {
            Destroy(gameObject);
        }
    }
}

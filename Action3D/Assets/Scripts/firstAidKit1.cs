using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firstAidKit1 : MonoBehaviour
{
    public Animator animator;
    public float timer;
    public bool timerbool;
    public bool timerboolSecond;
    public GameObject text;
    public GameObject count;
    public GameObject GameObject1;

    public AudioClip openAudio;
    public AudioSource audiosource;

    public bool first = false;

    public void Start()
    {
        GameObject1 = transform.parent.gameObject;
        animator = transform.parent.gameObject.GetComponent<Animator>();
    }
    public void OnTriggerStay(Collider other)
    {
        var countInt = GetComponent<firstAidKitController>();
        countInt.enabled = true;
        if (timerboolSecond == true)
        {
            if (timer > 1)
            {
                timerboolSecond = false;
                timerbool = false;
                timer = 0;
                gameObject.GetComponent<firstAidKitController>().quantity--;
                other.gameObject.GetComponent<Player_Controller>().hp = 50;
                if (first == false)
                {
                    first = true;
                    audiosource.PlayOneShot(openAudio);
                    animator.Play("Open");
                }
            }
        }
        if (timerbool == true)
        {
            timer += Time.deltaTime;
        }
        if (other.gameObject.tag == "Player")
        {
            text.SetActive(true);
            count.SetActive(true);
            if (Input.GetKey(KeyCode.H))
            {
                if (other.gameObject.GetComponent<Player_Controller>().hp < 50)
                {
                    if (gameObject.GetComponent<firstAidKitController>().quantity > 0)
                    {
                        timerboolSecond = true;
                        timerbool = true;
                    }
                }
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            text.SetActive(false);
            count.SetActive(false);
            var countInt = GetComponent<firstAidKitController>();
            countInt.enabled = false;
        }
    }
}
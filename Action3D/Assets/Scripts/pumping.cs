using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pumping : MonoBehaviour
{
    public GameObject Player;
    public GameObject Is_Activity;
    public GameObject spawn;

    public Object Shotgun2;
    public Object Shotgun3;
    public Object pistol2;
    public Object pistol3;
    public Object Submachine;
    public Object assault2;
    public Object grenade;

    public Sprite sprite;

    public float timer;

    public bool update;

    public bool key;

    public GameObject canvas;
    public Image bar;
    public float barInt = 0;
    public GameObject text;

    public void Update()
    {
        Is_Activity = Player.GetComponent<selected_slot>().IsActivity;

        timer += Time.deltaTime;
        bar.fillAmount = barInt;
    }
    public void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            text.SetActive(true);
            if(Is_Activity.GetComponent<Slot>().InSlot == "Shotgun1")
            {
                if (Player.GetComponent<material>().materialInt >= 15)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                        canvas.SetActive(true);

                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 15;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Player.GetComponent<Shotgun2>().AmmoInGun = Player.GetComponent<Shotgun1>().AmmoInGun;
                        Instantiate(Shotgun2, spawn.transform.position, spawn.transform.rotation);
                    }
                }
            }
            if(Is_Activity.GetComponent<Slot>().InSlot == "Shotgun2")
            {
                if (Player.GetComponent<material>().materialInt >= 15)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 15;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Instantiate(Shotgun3, spawn.transform.position, spawn.transform.rotation);
                        Player.GetComponent<Shotgun>().AmmoInGun = Player.GetComponent<Shotgun2>().AmmoInGun;
                    }
                }
            }
            if (Is_Activity.GetComponent<Slot>().InSlot == "pistol")
            {
                if (Player.GetComponent<material>().materialInt >= 10)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 10;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Instantiate(pistol2, spawn.transform.position, spawn.transform.rotation);
                        Player.GetComponent<Pistol_with_muffler>().AmmoInGun = Player.GetComponent<Pistol_Controller>().AmmoInGun;
                    }
                }
            }
            if (Is_Activity.GetComponent<Slot>().InSlot == "pistol2")
            {
                if (Player.GetComponent<material>().materialInt >= 10)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 10;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Instantiate(pistol3, spawn.transform.position, spawn.transform.rotation);
                        Player.GetComponent<Revolver_Controller>().AmmoInGun = Player.GetComponent<Pistol_with_muffler>().AmmoInGun;
                    }
                }
            }
            if (Is_Activity.GetComponent<Slot>().InSlot == "Uzi")
            {
                if (Player.GetComponent<material>().materialInt >= 30)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 30;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Instantiate(Submachine, spawn.transform.position, spawn.transform.rotation);
                        Player.GetComponent<Submachine>().AmmoInGun = Player.GetComponent<Uzi>().AmmoInGun;
                    }
                }
            }
            if (Is_Activity.GetComponent<Slot>().InSlot == "assault rifle level 1")
            {
                if (Player.GetComponent<material>().materialInt >= 15)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 30;
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Instantiate(assault2, spawn.transform.position, spawn.transform.rotation);
                        Player.GetComponent<assault2>().AmmoInGun = Player.GetComponent<assault1>().AmmoInGun;
                    }
                }
            }
            if (Is_Activity.GetComponent<Slot>().InSlot == "grenade")
            {
                if (Player.GetComponent<material>().materialInt >= 20)
                {
                    if (key == true)
                    {
                        barInt += 0.01f;
                        if (timer > 2)
                        {
                            update = true;
                        }
                    }
                    if (Input.GetKey(KeyCode.U))
                    {
                        canvas.SetActive(true);
                        timer = 0;
                        key = true;
                        Player.GetComponent<Player_Controller>().enabled = false;
                    }
                    if (update == true)
                    {
                        barInt = 0;
                        canvas.SetActive(false);
                        update = false;
                        key = false;
                        Player.GetComponent<Player_Controller>().enabled = true;
                        Player.GetComponent<material>().materialInt -= 20;
                        if (Player.GetComponent<grenadeController>().count == 1)
                        {
                            Is_Activity.GetComponent<Image>().sprite = sprite;
                            Is_Activity.GetComponent<Slot>().IsFull = false;
                            Is_Activity.GetComponent<Slot>().InSlot = null;
                        }
                        else
                        {
                            Player.GetComponent<grenadeController>().count--;
                        }
                        Instantiate(grenade, spawn.transform.position, spawn.transform.rotation);
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        text.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class material : MonoBehaviour
{
    public int materialInt;
    public float range;
    public Transform Camera;
    public GameObject int_text;
    public LayerMask layerMask;
    public GameObject text;

    public GameObject Panel1Material;
    public GameObject Panel2Material;
    public GameObject Panel3Material;
    public GameObject Panel4Material;
    public GameObject Panel5Material;

    public void Start()
    {
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 1)
        {
            int_text = Panel1Material;
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 2)
        {
            int_text = Panel2Material;
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 3)
        {
            int_text = Panel3Material;
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 4)
        {
            int_text = Panel4Material;
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 5)
        {
            int_text = Panel5Material;
        }
    }

    void Update()
    {
        if (materialInt >= 45)
        {
            materialInt = 45;
        }
        int_text.GetComponent<Text>().text = materialInt.ToString();
        RaycastHit hit;
        if (Physics.Raycast(Camera.position, Camera.transform.forward, out hit, range, layerMask))
        {
            if (hit.collider.gameObject.tag == "material")
            {
                text.SetActive(true);
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    if (materialInt != 45)
                    {
                        materialInt += Random.Range(1, 15);
                        Destroy(hit.collider.gameObject);
                    }
                }
            }
            else
            {
                if (hit.collider.gameObject.tag != "Ammo_box")
                {
                    text.SetActive(false);
                }
            }
        }
    }
}

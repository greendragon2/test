using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public bool IsFull;
    public Sprite flashlight;
    public Sprite pistol;
    public Sprite pistol2;
    public Sprite Ak;
    public Sprite Knife;
    public Sprite Revolver;
    public Sprite Automatic;
    public Sprite grenade;
    public Sprite Shotgun3;
    public Sprite Shotgun2;
    public Sprite Shotgun1;
    public Sprite uzi;
    public Sprite Submachine;
    public Sprite assault1;
    public Sprite assault2;
    public Sprite water;
    public Sprite food;
    public Sprite wireless_bomb;
    public GameObject slot;
    public string InSlot = null;
    public Color Color;
    public GameObject quantity;

    public GameObject Player;

    public GameObject Is_Activity;

    public Color ColorWhite;
    public bool quantityBool;


    private void Start()
    {
        InSlot = null;
    }
    public void Update()
    {
        Is_Activity = Player.GetComponent<selected_slot>().IsActivity;
        if (Is_Activity.name != slot.name)
        {
            if (InSlot != null)
            {
                slot.GetComponent<Image>().color = Color.white;
            }
        }

        if (InSlot == "grenade")
        {
            quantity.SetActive(true);
            quantity.GetComponent<Text>().text = Player.GetComponent<grenadeController>().count.ToString();
            quantityBool = true;
        }
        if (InSlot == "wireless bomb")
        {
            quantity.SetActive(true);
            quantity.GetComponent<Text>().text = Player.GetComponent<wireless_bomb>().count.ToString();
            quantityBool = true;
        }
        else if (InSlot == "food")
        {
            quantity.SetActive(true);
            quantity.GetComponent<Text>().text = Player.GetComponent<food>().count.ToString();
            quantityBool = true;
        }
        else
        {
            if (InSlot == null)
            {
                quantityBool = false;
            }
            if (quantityBool == false)
            {
                quantity.SetActive(false);
            }
        }
    }
    public void add_item(string name)
    {
        if (IsFull == false)
        {
            IsFull = true;
            if (name == "Flashlight")
            {
                InSlot = "Flashlight";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = flashlight;
            }
            else if (name == "pistol")
            {
                InSlot = "pistol";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = pistol;
            }
            else if (name == "pistol2")
            {
                InSlot = "pistol2";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = pistol2;
            }
            else if (name == "Ak")
            {
                InSlot = "Ak";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Ak;
            }
            else if (name == "Knife")
            {
                InSlot = "Knife";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Knife;
            }
            else if (name == "Revolver")
            {
                InSlot = "Revolver";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Revolver;
            }
            else if (name == "Automatic")
            {
                InSlot = "Automatic";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Automatic;
            }
            else if (name == "Shotgun3")
            {
                InSlot = "Shotgun3";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Shotgun3;
            }
            else if (name == "Shotgun2")
            {
                InSlot = "Shotgun2";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Shotgun2;
            }
            else if (name == "Shotgun1")
            {
                InSlot = "Shotgun1";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Shotgun1;
            }
            else if (name == "grenade")
            {
                InSlot = "grenade";
                gameObject.GetComponent<Image>().color = Color;
                GetComponent<Image>().sprite = grenade;
                //quantity.SetActive(true);
            }
            else if (name == "Uzi")
            {
                InSlot = "Uzi";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = uzi;
            }
            else if (name == "Submachine-gun")
            {
                InSlot = "Submachine-gun";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = Submachine;
            }
            else if (name == "assault rifle level 1")
            {
                InSlot = "assault rifle level 1";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = assault1;
            }
            else if (name == "assault rifle level 2")
            {
                InSlot = "assault rifle level 2";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = assault2;
            }
            else if (name == "water")
            {
                InSlot = "water";
                gameObject.GetComponent<Image>().color = Color;
                quantityBool = false;
                GetComponent<Image>().sprite = water;
            }
            else if (name == "wireless bomb")
            {
                InSlot = "wireless bomb";
                gameObject.GetComponent<Image>().color = Color;
                GetComponent<Image>().sprite = wireless_bomb;
            }
            else if (name == "food")
            {
                InSlot = "food";
                gameObject.GetComponent<Image>().color = Color;

                GetComponent<Image>().sprite = food;
            }
            else
            {
                InSlot = null;
            }
        }
    }
}

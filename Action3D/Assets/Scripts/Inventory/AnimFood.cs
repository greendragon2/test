using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimFood : MonoBehaviour
{
    public Animator AnimatorFood;
    public AudioSource AudioSourceFood;
    public AudioClip AudioClipFood;
    public bool Food;
    public GameObject player;
    public float timer; 
    public float timer1;
    public bool timerBool;
    public GameObject slot;
    void Update()
    {
        if (player.GetComponent<InventoryController>().add_object == false)
        {
            slot = player.GetComponent<selected_slot>().IsActivity;
            if (timerBool == true)
            {
                timer += Time.deltaTime;
                if (timer > 2)
                {
                    timer = 0;
                    timerBool = false;
                    player.GetComponent<food>().count--;
                    AnimatorFood.Play("food1");
                    Food = false;
                }
            }
            if (Input.GetKeyUp(KeyCode.Mouse0) && Food == false)
            {
                AnimatorFood.Play("food");
                AudioSourceFood.PlayOneShot(AudioClipFood);
                Food = true;
                timerBool = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && Food == true)
            {
                timer = 0;
                timerBool = false;
                AnimatorFood.Play("food1");
                Food = false;
            }
            if (player.GetComponent<food>().InSlot == false)
            {
                AnimatorFood.Play("food2");
                timer1 += Time.deltaTime;
                if (timer1 > 0.1)
                {
                    Food = !Food;
                    timer1 = 0;
                    gameObject.SetActive(false);
                }
            }
        }
    }
}   
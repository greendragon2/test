using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    public GameObject text;
    public GameObject item;
    public Slot slot1;
    public Slot slot2;
    public Slot slot3;
    public Slot slot4;
    public Slot slot5;
    public string name;
    public GameObject Slot1;
    public GameObject Slot2;
    public GameObject Slot3;
    public GameObject Slot4;
    public GameObject Slot5;
    public GameObject Is_Activity;
    public GameObject IsNotFull_text;
    public float time;
    public Slot IsNotFull;
    public bool IsNotFull_bool;
    public Object Pistol;
    public Object Ak;
    public Object FlashlightPrefab;
    public Object Knife;
    public Object grenade;
    public Object Shotgun3;
    public Object Shotgun2;
    public Object Shotgun1;
    public Object Pistol2;
    public Object Uzi;
    public Object Submachine;
    public Object assault1;
    public Object water;
    public Object assault2;
    public Object food;
    public Object WirelessBombObject;
    public Sprite sprite;
    public int range;
    public Transform Camera;
    public bool coincidence;
    public bool maradurAkBool;
    public Object Revolver;
    public Object Automatic;
    public GameObject NoCharging;
    public GameObject slot;
    public bool Add;

    public int maradurAk;

    public LayerMask layerMask;
    public bool add_object;

    //public List<string> inventory = new List<string>();}
    public void Start()
    {
        slot1 = Slot1.GetComponent<Slot>();
        slot2 = Slot2.GetComponent<Slot>();
        slot3 = Slot3.GetComponent<Slot>();
        slot4 = Slot4.GetComponent<Slot>();
        slot5 = Slot5.GetComponent<Slot>();
    }
    public void Update()
    {
        slot = gameObject.GetComponent<selected_slot>().IsActivity;
        if (slot1.IsFull == false)
        {
            IsNotFull = slot1;
        }
        else if (slot2.IsFull == false)
        {
            IsNotFull = slot2;
        }
        else if (slot3.IsFull == false)
        {
            IsNotFull = slot3;
        }
        else if (slot4.IsFull == false)
        {
            IsNotFull = slot4;
        }
        else if (slot5.IsFull == false)
        {
            IsNotFull = slot5;
        }
        else
        {
            IsNotFull = null;
        }

        RaycastHit hit;
        if (Physics.Raycast(Camera.position, Camera.transform.forward, out hit, range, layerMask))
        {
            if (hit.collider.gameObject.tag == "item")
            {
                add_object = true;
                Add = false;
                text.SetActive(true);
                item.GetComponent<Text>().text = hit.collider.gameObject.name;
                if (Input.GetKeyUp(KeyCode.Mouse0))
                {
                    if (IsNotFull != null)
                    {
                        item.GetComponent<Text>().text = "";
                        text.SetActive(false);
                        name = hit.collider.gameObject.name;
                        if (name == "Kalashnikov assault rifle")
                        {
                            name = "Ak";
                        }
                        if (name == "Shotgun level 3")
                        {
                            name = "Shotgun3";
                        }
                        if (name == "Shotgun level 2")
                        {
                            name = "Shotgun2";
                        }
                        if (name == "Shotgun level 1")
                        {
                            name = "Shotgun1";
                        }
                        if (name == "Pistol with muffler")
                        {
                            name = "pistol2";
                        }
                        if (name == "bottle water")
                        {
                            name = "water";
                        }
                        if (slot1.InSlot == name)
                        {
                            coincidence = true;
                            Add = false;
                        }
                        if (slot2.InSlot == name)
                        {
                            coincidence = true;
                            Add = false;
                        }
                        if (slot3.InSlot == name)
                        {
                            coincidence = true;
                            Add = false;
                        }
                        if (slot4.InSlot == name)
                        {
                            coincidence = true;
                            Add = false;
                        }
                        if (slot5.InSlot == name)
                        {
                            coincidence = true;
                            Add = false;
                        }

                        if (coincidence == false)
                        {
                            IsNotFull.add_item(name);
                            Add = true;
                            if (hit.collider.gameObject.GetComponent<maradurAk>() != null)
                            {
                                maradurAkBool = true;
                                GetComponent<Ak>().AmmoInGun = hit.collider.gameObject.GetComponent<maradurAk>().AmmoInGun;
                                maradurAk = hit.collider.gameObject.GetComponent<maradurAk>().AmmoInGun;
                            }
                            Destroy(hit.collider.gameObject);
                        }

                        if (coincidence == true)
                        {
                            if (hit.collider.gameObject.GetComponent<maradurAk>() != null)
                            {
                                maradurAkBool = true;
                                maradurAk = hit.collider.gameObject.GetComponent<maradurAk>().AmmoInGun;
                            }
                            if (name == "pistol")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 30);
                                    Add = true;
                                }
                            }
                            if (name == "pistol2")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 30);
                                    Add = true;
                                }
                            }
                            if (name == "Ak")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    if (maradurAkBool == true)
                                    {
                                        gameObject.GetComponent<Ammo_box>().ammoIntMachine += maradurAk;
                                        maradurAkBool = false;
                                    }
                                    else
                                    {
                                        gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                    }
                                    Add = true;
                                }
                            }
                            if (name == "Flashlight")
                            {
                                if (battery.batteries != 30)
                                {
                                    Destroy(hit.collider.gameObject);
                                    battery.batteries += Random.Range(0, 3);
                                }
                            }
                            if (name == "Revolver")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 9);
                                    Add = true;
                                }
                            }
                            if (name == "Automatic")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Add = true;
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                }
                            }
                            if (name == "Shotgun3")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 3);
                                    Add = true;
                                }
                            }
                            if (name == "Shotgun2")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 2);
                                    Add = true;
                                }
                            }
                            if (name == "Shotgun1")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 2);
                                    Add = true;
                                }
                            }
                            if (name == "Uzi")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 100);
                                    Add = true;
                                }
                            }
                            if (name == "Submachine-gun")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 40);
                                    Add = true;
                                }
                            }
                            if (name == "assault rifle level 1")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                    Add = true;
                                }
                            }
                            if (name == "assault rifle level 2")
                            {
                                if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 60);
                                    Add = true;
                                }
                            }
                            if (name == "water")
                            {
                                if (gameObject.GetComponent<bottleWater>().left < 100)
                                {
                                    Destroy(hit.collider.gameObject);
                                    gameObject.GetComponent<bottleWater>().left += Random.Range(0, 100);
                                    Add = true;
                                    if (gameObject.GetComponent<bottleWater>().left > 100)
                                    {
                                        gameObject.GetComponent<bottleWater>().left = 100;
                                    }
                                }
                            }
                            if (name == "grenade")
                            {
                                if (GetComponent<grenadeController>().count < 32)
                                {
                                    Destroy(hit.collider.gameObject);
                                    GetComponent<grenadeController>().count ++;
                                    Add = true;
                                }
                            }
                            if (name == "wireless bomb")
                            {
                                if (GetComponent<grenadeController>().count < 32)
                                {
                                    Destroy(hit.collider.gameObject);
                                    GetComponent<wireless_bomb>().count++;
                                    Add = true;
                                }
                            }
                            if (name == "food")
                            {
                                if (GetComponent<grenadeController>().count < 32)
                                {
                                    Destroy(hit.collider.gameObject);
                                    GetComponent<food>().count++;
                                    Add = true;
                                }
                            }
                            coincidence = false;
                        }
                    }
                    if (Add == false)
                    {
                        if (hit.collider.gameObject.GetComponent<maradurAk>() != null)
                        {
                            maradurAkBool = true;
                            maradurAk = hit.collider.gameObject.GetComponent<maradurAk>().AmmoInGun;
                        }
                        if (name == "pistol")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 30);
                            }
                        }
                        if (name == "pistol2")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 30);
                                Add = true;
                            }
                        }
                        if (name == "Ak")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                if (maradurAkBool == true)
                                {
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += maradurAk;
                                    maradurAkBool = false;
                                }
                                else
                                {
                                    gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                }
                            }
                        }
                        if (name == "Flashlight")
                        {
                            if (battery.batteries != 30)
                            {
                                Destroy(hit.collider.gameObject);
                                battery.batteries += Random.Range(0, 3);
                            }
                        }
                        if (name == "Revolver")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntPistol != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntPistol += Random.Range(0, 9);
                            }
                        }
                        if (name == "Automatic")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                            }
                        }
                        if (name == "Shotgun3")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 3);
                            }
                        }
                        if (name == "Shotgun2")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 3);
                            }
                        }
                        if (name == "Shotgun1")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntShotgun != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntShotgun += Random.Range(0, 2);
                                Add = true;
                            }
                        }
                        if (name == "Uzi")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 100);
                                Add = true;
                            }
                        }
                        if (name == "Submachine-gun")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                Add = true;
                            }
                        }
                        if (name == "assault rifle level 1")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 30);
                                Add = true;
                            }
                        }
                        if (name == "assault rifle level 2")
                        {
                            if (gameObject.GetComponent<Ammo_box>().ammoIntMachine != 999)
                            {
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<Ammo_box>().ammoIntMachine += Random.Range(0, 60);
                                Add = true;
                            }
                        }
                        if (name == "grenade")
                        {
                            if (GetComponent<grenadeController>().count < 32)
                            {
                                Destroy(hit.collider.gameObject);
                                GetComponent<grenadeController>().count ++;
                            }
                        }
                        if (name == "wireless bomb")
                        {
                            if (GetComponent<grenadeController>().count < 32)
                            {
                                Destroy(hit.collider.gameObject);
                                GetComponent<wireless_bomb>().count++;
                                Add = true;
                            }
                        }
                        if (name == "food")
                        {
                            if (GetComponent<grenadeController>().count < 32)
                            {
                                Destroy(hit.collider.gameObject);
                                GetComponent<food>().count ++;
                            }
                        }
                        if (name == "water")
                        {
                            if (gameObject.GetComponent<bottleWater>().left < 100)
                            {
                                print(gameObject.GetComponent<bottleWater>().left);
                                Destroy(hit.collider.gameObject);
                                gameObject.GetComponent<bottleWater>().left += Random.Range(0, 100);
                                Add = true;
                                if (gameObject.GetComponent<bottleWater>().left > 100)
                                {
                                    gameObject.GetComponent<bottleWater>().left = 100;
                                }
                            }
                        }
                    }
                }
            }
            else if (hit.collider.gameObject.tag == "Ammo_box")
            {
                item.GetComponent<Text>().text = "Ammo box";
            }
            else if (hit.collider.gameObject.tag == "material")
            {
                item.GetComponent<Text>().text = "Material";
            }
            else if (hit.collider.gameObject.tag == "first aid kit")
            {
                item.GetComponent<Text>().text = "first aid kit";
            }
            else
            {
                item.GetComponent<Text>().text = "";
                text.SetActive(false);
                add_object = false;
            }
        }
        if (IsNotFull_bool == true)
        {
            time += Time.deltaTime;
            if (time >= 1.5)
            {
                IsNotFull_bool = false;
                IsNotFull_text.SetActive(false);
                time = 0;
            }
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            Is_Activity = GetComponent<selected_slot>().IsActivity;
            if (Is_Activity.GetComponent<Slot>().InSlot == "Flashlight")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                gameObject.GetComponent<selected_slot>().q = true;
                Instantiate(FlashlightPrefab, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "pistol")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Pistol, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "pistol2")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Pistol2, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Ak")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                NoCharging.SetActive(false);
                Instantiate(Ak, transform.position, transform.rotation);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Knife")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Knife, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Revolver")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Revolver, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Automatic")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Automatic, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Shotgun3")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Shotgun3, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Shotgun2")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Shotgun2, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Shotgun1")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Shotgun1, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Uzi")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Uzi, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "Submachine-gun")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(Submachine, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "assault rifle level 1")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(assault1, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "assault rifle level 2")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(assault2, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "water")
            {
                Is_Activity.GetComponent<Image>().sprite = sprite;
                Instantiate(water, transform.position, transform.rotation);
                NoCharging.SetActive(false);
                Is_Activity.GetComponent<Slot>().InSlot = null;
                Is_Activity.GetComponent<Slot>().IsFull = false;
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "grenade")
            {
                if (gameObject.GetComponent<grenadeController>().count == 1)
                {
                    gameObject.GetComponent<grenadeController>().count = 0;
                    Instantiate(grenade, transform.position, transform.rotation);
                    Is_Activity.GetComponent<Slot>().InSlot = null;
                    Is_Activity.GetComponent<Slot>().IsFull = false;
                }
                else
                {
                    gameObject.GetComponent<grenadeController>().count -= 1;
                    Instantiate(grenade, transform.position, transform.rotation);
                }
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "wireless bomb")
            {
                if (gameObject.GetComponent<wireless_bomb>().count == 1)
                {
                    gameObject.GetComponent<wireless_bomb>().count = 0;
                    Instantiate(WirelessBombObject, transform.position, transform.rotation);
                    Is_Activity.GetComponent<Slot>().InSlot = null;
                    Is_Activity.GetComponent<Slot>().IsFull = false;
                }
                else
                {
                    gameObject.GetComponent<wireless_bomb>().count -= 1;
                    Instantiate(WirelessBombObject, transform.position, transform.rotation);
                }
            }
            else if (Is_Activity.GetComponent<Slot>().InSlot == "food")
            {
                if (gameObject.GetComponent<food>().count == 0)
                {
                    //gameObject.GetComponent<food>().count = 0;
                    Instantiate(food, transform.position, transform.rotation);
                    Is_Activity.GetComponent<Slot>().InSlot = null;
                    Is_Activity.GetComponent<Slot>().IsFull = false;
                }
                else
                {
                    gameObject.GetComponent<food>().count -= 1;
                    Instantiate(food, transform.position, transform.rotation);
                }
            }
        }
    }
}
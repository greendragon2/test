using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selected_slot : MonoBehaviour
{
    public Color Color_item;
    public Color Color_default;
    public Color Color;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;
    public GameObject slot4;
    public GameObject slot5;

    public GameObject Flashlight;

    public GameObject IsActivity;

    public bool q;

    public int slot = 1;

    public bool pause;

    private void Start()
    {
        IsActivity = slot1;
    }

    private void Update()
    {
        if (pause == false)
        {
            if (IsActivity.GetComponent<Slot>().InSlot == "Flashlight")
            {
                Flashlight.SetActive(true);
            }
            else
            {
                Flashlight.SetActive(false);
            }

            if (Input.GetAxis("Mouse ScrollWheel") >= 0.1)
            {
                if (slot >= 5)
                {
                    slot = 1;
                }
                else
                {
                    slot += 1;
                }
            }
            else if (Input.GetAxis("Mouse ScrollWheel") <= -0.1)
            {
                if (slot <= 1)
                {
                    slot = 5;
                }
                else
                {
                    slot -= 1;
                }
            }
            if (slot == 1)
            {
                IsActivity = slot1;
                slot1.GetComponent<Image>().color = Color;
                slot2.GetComponent<Image>().color = Color_default;
                slot3.GetComponent<Image>().color = Color_default;
                slot4.GetComponent<Image>().color = Color_default;
                slot5.GetComponent<Image>().color = Color_default;
            }
            else if (slot == 2)
            {
                IsActivity = slot2;
                slot2.GetComponent<Image>().color = Color;
                slot1.GetComponent<Image>().color = Color_default;
                slot3.GetComponent<Image>().color = Color_default;
                slot4.GetComponent<Image>().color = Color_default;
                slot5.GetComponent<Image>().color = Color_default;
            }
            else if (slot == 3)
            {
                IsActivity = slot3;
                slot3.GetComponent<Image>().color = Color;
                slot1.GetComponent<Image>().color = Color_default;
                slot2.GetComponent<Image>().color = Color_default;
                slot4.GetComponent<Image>().color = Color_default;
                slot5.GetComponent<Image>().color = Color_default;
            }
            else if (slot == 4)
            {
                slot4.GetComponent<Image>().color = Color;
                slot1.GetComponent<Image>().color = Color_default;
                slot3.GetComponent<Image>().color = Color_default;
                slot2.GetComponent<Image>().color = Color_default;
                slot5.GetComponent<Image>().color = Color_default;
                IsActivity = slot4;
            }
            else if (slot == 5)
            {

                slot5.GetComponent<Image>().color = Color;
                slot1.GetComponent<Image>().color = Color_default;
                slot3.GetComponent<Image>().color = Color_default;
                slot4.GetComponent<Image>().color = Color_default;
                slot2.GetComponent<Image>().color = Color_default;
                IsActivity = slot5;
            }
            if (q == true)
            {
                Flashlight.SetActive(false);
                q = false;
            }
        }
    }
}
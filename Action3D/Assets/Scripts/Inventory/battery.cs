using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class battery : MonoBehaviour
{
    public static int batteries = 0;
    public GameObject text;
    public GameObject textBatteries;

    public GameObject Panel1Batteries;
    public GameObject Panel2Batteries;
    public GameObject Panel3Batteries;
    public GameObject Panel4Batteries;
    public GameObject Panel5Batteries;

    public void Start()
    {
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 1)
        {
            text = Panel1Batteries;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 2)
        {
            text = Panel2Batteries;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 3)
        {
            text = Panel3Batteries;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 4)
        {
            text = Panel4Batteries;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 5)
        {
            text = Panel5Batteries;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "battery")
        {
            if(batteries != 30)
            {
                textBatteries.SetActive(true);
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    textBatteries.SetActive(false);
                    batteries += 1;
                    Destroy(other.gameObject);
                }
            }
        }
        else
        {
            textBatteries.SetActive(false);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        textBatteries.SetActive(false);
        if (other.gameObject.tag == "battery")
        {
            textBatteries.SetActive(false);
        }
    }
    public void Update()
    {
        text.GetComponent<Text>().text = batteries.ToString();
    }
}

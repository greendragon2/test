using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bottleWater : MonoBehaviour
{
    public GameObject player;
    public GameObject slot;
    public GameObject bottel;

    public float left;
    public GameObject LeftText;
    public bool water;
    public float water1;
    public float timer;
    public bool timerBool;

    public float left1;

    private void Start()
    {
        left = Random.Range(1, 100);
    }

    void Update()
    {
        if(timerBool == true)
        {
            timer += Time.deltaTime;
            if (left > 0)
            {
                if (water1 < 100)
                {
                    if (timer > 2)
                    {
                        timerBool = false;
                        timer = 0;
                        left1 = 100 - water1;
                        if (left1 < left)
                        {
                            GetComponent<Player_Controller>().waterInt += left1;
                            left -= left1;
                        }
                        else
                        {
                            GetComponent<Player_Controller>().waterInt += left;
                            left -= left;
                        }
                    }
                }
            }
        }
        water1 = GetComponent<Player_Controller>().waterInt;
        water = bottel.GetComponent<AnimWater>().water;
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (slot.GetComponent<Slot>().InSlot == "water")
        {
            LeftText.SetActive(true);
            LeftText.GetComponent<Text>().text = Mathf.RoundToInt(left).ToString();
            bottel.SetActive(true);
            if(gameObject.GetComponent<InventoryController>().add_object == false)
            {
                if (Input.GetKeyUp(KeyCode.Mouse0) && water == false)
                {
                    timerBool = true;
                }
                if (Input.GetKeyUp(KeyCode.Mouse0) && water == true)
                {
                    timerBool = false;
                    timer = 0;
                }
            }
        }
        else
        {
            bottel.SetActive(false);
            LeftText.SetActive(false);
            timerBool = false;
            timer = 0;
        }
    }
}
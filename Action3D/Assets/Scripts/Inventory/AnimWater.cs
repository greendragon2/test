using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimWater : MonoBehaviour
{
    public Animator AnimatorWater;
    public bool water;
    public AudioSource audioSource;
    public float timer;
    public bool timerBool;
    public GameObject Player;
    void Update()
    {
        if (timerBool == true)
        {
            timer += Time.deltaTime;
            if (timer > 1)
            {
                if (Player.GetComponent<bottleWater>().left>0)
                {
                    Player.GetComponent<bottleWater>().left -= 2;
                    timerBool = false;
                    timer = 0;
                    AnimatorWater.Play("water1");
                    water = false;
                    audioSource.Stop();
                }
            }
        }
        if (Player.GetComponent<InventoryController>().add_object == false)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0) && water == false)
            {
                if (Player.GetComponent<bottleWater>().left > 0)
                {
                    audioSource.Play();
                }
                AnimatorWater.Play("water");
                water = true;
                timerBool = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && water == true)
            {
                AnimatorWater.Play("water1");
                water = false;
                audioSource.Stop();
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flashlight : MonoBehaviour
{
    public GameObject slot;
    public GameObject charge;
    public float charge_int = 100;
    public GameObject player;
    public battery player_script;
    public float time;
    public float timeAddBattery;
    public GameObject chargetext;
    public bool add;
    public GameObject lashlight;
    public bool light_on = false;
    public AudioSource AudioSource;
    public AudioClip Add;
    public AudioClip OnAndOff;
    public bool AddBettery;
    public float timer;

    private void Update()
    {
        timer += Time.deltaTime;
        timeAddBattery += Time.deltaTime;
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (add)
        {
            if (timeAddBattery > 1)
            {
                if (timer > 4)
                {
                    timer = 0;
                    battery.batteries -= 3;
                    charge_int = 100;
                    timeAddBattery = 0;
                    add = false;
                }
            }
        }
        if (slot.GetComponent<Slot>().InSlot == "Flashlight")
        {
            if(player.GetComponent<InventoryController>().add_object == false)
            {
                if (Input.GetKeyUp(KeyCode.Mouse0) & light_on == false)
                {
                    AddBettery = false;
                    AudioSource.PlayOneShot(OnAndOff);
                    if (charge_int! >= 0)
                    {
                        lashlight.SetActive(true);
                        light_on = true;
                    }
                }
                else if (Input.GetKeyUp(KeyCode.Mouse0) & light_on == true)
                {
                    AddBettery = false;
                    AudioSource.PlayOneShot(OnAndOff);
                    lashlight.SetActive(false);
                    light_on = false;
                }
            }

            if (light_on == true)
            {
                time = Time.deltaTime;
                charge_int -= time;
            }

            if (charge_int <= 0)
            {
                charge.GetComponent<Text>().text = "0";
            }
            else
            {
                charge.GetComponent<Text>().text = Mathf.RoundToInt(charge_int).ToString();
            }

            charge.SetActive(true);

            if (charge_int <= 0)
            {
                lashlight.SetActive(false);
                light_on = false;
                if(battery.batteries >= 3)
                {
                    chargetext.SetActive(true);
                    if (Input.GetKey(KeyCode.X))
                    {
                        if (AddBettery == false)
                        {
                            AddBettery = true;
                            AudioSource.PlayOneShot(Add);

                        }
                        timeAddBattery = 0;
                        add = true;
                        timer = 0;
                        chargetext.SetActive(false);
                    }
                }
            }
        }
        else
        {
            charge.SetActive(false);
        }
    }
    private void Start()
    {
        lashlight.SetActive(false);
    }
}

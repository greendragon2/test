using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo_box : MonoBehaviour
{
    public int ammoIntPistol;
    public GameObject ammoIntPistolText;
    public int ammoIntMachine;
    public GameObject ammoIntMachineText;
    public int ammoIntShotgun;
    public GameObject ammoIntShotgunText;

    public Transform Camera;
    public int range;
    public LayerMask layerMask;

    public GameObject text;

    public GameObject Panel1Pistol;
    public GameObject Panel2Pistol;
    public GameObject Panel3Pistol;
    public GameObject Panel4Pistol;
    public GameObject Panel5Pistol;

    public GameObject Panel1Machine;
    public GameObject Panel2Machine;
    public GameObject Panel3Machine;
    public GameObject Panel4Machine;
    public GameObject Panel5Machine;

    public GameObject Panel1Shotgun;
    public GameObject Panel2Shotgun;
    public GameObject Panel3Shotgun;
    public GameObject Panel4Shotgun;
    public GameObject Panel5Shotgun;

    private void Start()
    {
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 1)
        {
            ammoIntPistolText = Panel1Pistol;
            ammoIntMachineText = Panel1Machine;
            ammoIntShotgunText = Panel1Shotgun;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 2)
        {
            ammoIntPistolText = Panel2Pistol;
            ammoIntMachineText = Panel2Machine;
            ammoIntShotgunText = Panel2Shotgun;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 3)
        {
            ammoIntPistolText = Panel3Pistol;
            ammoIntMachineText = Panel3Machine;
            ammoIntShotgunText = Panel3Shotgun;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 4)
        {
            ammoIntPistolText = Panel4Pistol;
            ammoIntMachineText = Panel4Machine;
            ammoIntShotgunText = Panel4Shotgun;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 5)
        {
            ammoIntPistolText = Panel5Pistol;
            ammoIntMachineText = Panel5Machine;
            ammoIntShotgunText = Panel5Shotgun;
        }
        else if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 1)
        {
            ammoIntPistolText = Panel1Pistol;
            ammoIntMachineText = Panel1Machine;
            ammoIntShotgunText = Panel1Shotgun;
        }
    }

    public void Update()
    {
        if (ammoIntPistol >= 999)
        {
            ammoIntPistol = 999;
        }
        if (ammoIntMachine >= 999)
        {
            ammoIntMachine = 999;
        }
        if (ammoIntShotgun >= 999)
        {
            ammoIntShotgun = 999;
        }
        ammoIntPistolText.GetComponent<Text>().text = ammoIntPistol.ToString();
        ammoIntMachineText.GetComponent<Text>().text = ammoIntMachine.ToString();
        ammoIntShotgunText.GetComponent<Text>().text = ammoIntShotgun.ToString();
        RaycastHit hit;
        if (Physics.Raycast(Camera.position, Camera.transform.forward, out hit, range, layerMask))
        {
            if (hit.collider.gameObject.tag == "Ammo_box")
            {
                text.SetActive(true);
                if (Input.GetKey(KeyCode.Mouse1))
                {
                    if (ammoIntPistol < 999)
                    {
                        ammoIntPistol += Random.Range(0, 50);
                    }
                    if (ammoIntMachine < 999)
                    {
                        ammoIntMachine += Random.Range(0, 50);
                    }
                    if (ammoIntShotgun < 999)
                    {
                        ammoIntShotgun += Random.Range(0, 9);
                    }
                    Destroy(hit.collider.gameObject);
                }
            }
            else
            {
                if (hit.collider.gameObject.tag != "material")
                {
                    text.SetActive(false);
                }
            }
        }
    }
}

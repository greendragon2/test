using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grenadeFlight : MonoBehaviour
{
    public int once;
    public float timer;
    public AudioSource AudioSource;
    public AudioClip AudioClip1;
    public AudioClip AudioClip2;
    public Object Effect;
    public bool first;
    public bool ColliderBox;
    public bool end;

    private void OnCollisionEnter(Collision collision)
    {
        ColliderBox = true;
    }

    void Start()
    {
        AudioSource.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioGun");
        var grenade = gameObject.GetComponent<SphereCollider>();
        grenade.enabled = false;
        AudioSource.PlayOneShot(AudioClip1);
    }

    // Update is called once per frame
    void Update()
    {
        if (end == true)
        {
            if (timer >= 0.05)
            {
                transform.localScale = new Vector3(0, 0, 0);
                Destroy(gameObject, 0.5f);
            }
        }
        timer += Time.deltaTime;
        if (once !< 120)
        {
            if (ColliderBox != true)
            transform.Translate(Vector3.forward * 25 * Time.deltaTime);
            once += 1;
        }
        if (timer >= 4.2)
        { 
            if (first != true)
            {
                first = true;
                AudioSource.PlayOneShot(AudioClip2);
                Instantiate(Effect, transform.position, transform.rotation);
            }
           var grenade = gameObject.GetComponent<SphereCollider>();
           grenade.enabled = true;
           timer = 0;
           end = true;   
        }
    }
}
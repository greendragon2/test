using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class wireless_bomb : MonoBehaviour
{
    public GameObject slot;
    public GameObject player;
    public GameObject GrenadeGameObject;
    public GameObject ControllerGameObject;
    public GameObject flight;
    public GameObject Smawn;
    public GameObject[] grenade;
    public Sprite sprite;
    public GameObject Is_Activity;
    public bool boom;
    public bool boomTrue;
    public List<GameObject> WirelessBomb;
    public bool On;
    public bool grenadeBool;


    public int count = 1;

    void Update()
    {
        if (boomTrue)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                boom = true;
            }
            else
            {
                boom = false;
            }
        }
        if (Input.GetKeyUp(KeyCode.R) && On == false)
        {
            ControllerGameObject.SetActive(false);
            On = true;
            grenadeBool = false;
            boomTrue = false;
        }
        else if (Input.GetKeyUp(KeyCode.R) && On == true)
        {
            On = false;
            ControllerGameObject.SetActive(true);
            GrenadeGameObject.SetActive(false);
            grenadeBool = true;
            boomTrue = true;
        }
        grenade = GameObject.FindGameObjectsWithTag("grenade");
        slot = player.GetComponent<selected_slot>().IsActivity;
        Is_Activity = GetComponent<selected_slot>().IsActivity;
        if (count <= 0)
        {
            Is_Activity.GetComponent<Image>().sprite = sprite;
            Is_Activity.GetComponent<Slot>().InSlot = null;
            Is_Activity.GetComponent<Slot>().IsFull = false;
            count = 1;
        }
        if (slot.GetComponent<Slot>().InSlot == "wireless bomb")
        {
            if (grenadeBool == false)
            {
                GrenadeGameObject.SetActive(true);
            }
            if (gameObject.GetComponent<InventoryController>().add_object == false)
            {
                if (GrenadeGameObject.activeSelf == true)
                {
                    if (Input.GetKeyUp(KeyCode.Mouse0))
                    {

                        if (count <= 0)
                        {
                            Is_Activity.GetComponent<Image>().sprite = sprite;
                            Is_Activity.GetComponent<Slot>().InSlot = null;
                            Is_Activity.GetComponent<Slot>().IsFull = false;
                            count = 1;
                        }
                        else
                        {
                            Instantiate(flight, Smawn.transform.position, Smawn.transform.rotation);
                        }
                        count--;
                    }
                }   
            }
            
        }
        else
        {
            GrenadeGameObject.SetActive(false);
        }
    }
}

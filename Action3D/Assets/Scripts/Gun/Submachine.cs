using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Submachine : MonoBehaviour
{
    public GameObject slot;
    public GameObject pistol1;
    public float damage;
    public float fireRate;
    public float range;
    public AudioClip shotSFX;
    public AudioSource audioSource;
    public AudioClip shotSFXnot_bullet;
    public AudioClip shotSFX_bullet_add;
    public Transform BulletSpawn;
    public Transform pistol_trnsform;
    public float force;
    private float nextFire = 0;
    public GameObject bullet;
    public GameObject hitEffect;
    public GameObject AmmoInGunText;
    public GameObject AmmoInGunText1;
    public GameObject hitIcon;
    public Ammo_box ammobox;
    public int ammo;
    public int AmmoInGun = 0;
    public int AmmoInGunNotAdd = 0;
    public GameObject player;
    public LayerMask layerMask;
    public bool AddAudio;
    public float time;
    public GameObject chargingtext;
    public bool trigger;
    void Update()
    {
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (slot.GetComponent<Slot>().InSlot == "Submachine-gun")
        {
            if (AmmoInGun == 0)
            {
                chargingtext.SetActive(true);
            }
            else
            {
                chargingtext.SetActive(false);
            }
            GetComponent<Player_Controller>().pistol = true;
            pistol1.SetActive(true);
            AmmoInGunText.SetActive(true);

            AmmoInGunText1.GetComponent<Text>().text = AmmoInGun.ToString();

            if (AmmoInGun > 50)
            {
                AmmoInGunNotAdd = AmmoInGun - 50;
                AmmoInGun = 50;
                ammobox.ammoIntMachine += AmmoInGunNotAdd;
            }

            if (AddAudio == true)
            {
                time += Time.deltaTime;
                if (time >= 2)
                {
                    ammobox.ammoIntMachine -= 50;
                    AmmoInGun += 50;
                    AddAudio = false;
                    time = 0;
                }
            }

            ammo = ammobox.ammoIntMachine;

            if (Input.GetKey(KeyCode.Mouse0) & Time.time > nextFire)
            {
                if (gameObject.GetComponent<InventoryController>().add_object == false)
                {
                    nextFire = Time.time + 1 / fireRate;
                    Shot();
                }
            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (AmmoInGun <= 0)
                {
                    if (ammo >= 0)
                    {
                        if (ammo >= 50)
                        {
                            audioSource.PlayOneShot(shotSFX_bullet_add);
                            AddAudio = true;
                        }
                    }
                }
            }
        }
        else
        {
            GetComponent<Player_Controller>().pistol = false;
            chargingtext.SetActive(false);
            pistol1.SetActive(false);
        }
    }
    void Shot()
    {
        if (AmmoInGun >= 1)
        {
            AmmoInGun -= 1;
            audioSource.PlayOneShot(shotSFX);
            Instantiate(bullet, BulletSpawn.position, BulletSpawn.rotation);
            if (trigger == false)
            {
                RaycastHit hit;
                if (Physics.Raycast(BulletSpawn.position, pistol_trnsform.transform.forward, out hit, range, layerMask))
                {
                    if (hit.rigidbody != null)
                    {
                        hit.rigidbody.AddForce(-hit.normal * force);
                        if (hit.collider.gameObject.tag == "Enemy")
                        {
                            hit.collider.gameObject.GetComponent<Enemy_Controller>().hp -= damage;
                        }
                        if (hit.collider.gameObject.tag == "marauder")
                        {
                            hit.collider.gameObject.GetComponent<hpEnemy>().hp -= damage;
                        }
                    }
                    if (hit.collider.gameObject.tag != "Enemy")
                    {
                        if (hit.collider.gameObject.tag != "marauder")
                        {
                            if (hit.collider.gameObject.tag != "bullet")
                            {
                                GameObject imact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                                GameObject imact1 = Instantiate(hitIcon, hit.point, Quaternion.LookRotation(hit.normal));
                                Destroy(imact, 5f);
                                imact1.transform.parent = hit.transform;
                                imact.transform.parent = hit.transform;
                            }
                        }
                    }
                }
            }
            else
            {
                audioSource.PlayOneShot(shotSFXnot_bullet);
            }
        }
        else
        {
            audioSource.PlayOneShot(shotSFXnot_bullet);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public GameObject player;
    public GameObject slot;
    public GameObject KnifeGameObject;
    public Animator KnifeAnimatorController;
    public bool On = false;
    public bool collider1;
    public float timer;
    void Update()
    {
        if (collider1 == true)
        {
            timer += Time.deltaTime;
            if (timer >= 0.2)
            {
                collider1 = false;
                var KnifeCollider = KnifeGameObject.GetComponent<BoxCollider>();
                KnifeCollider.enabled = false;
            }
        }
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (slot.GetComponent<Slot>().InSlot == "Knife")
        {
            KnifeGameObject.SetActive(true);
            if(Input.GetKeyUp(KeyCode.Mouse0) && On == false)
            {
                KnifeAnimatorController.Play("knife");
                On = true;
                var KnifeCollider = KnifeGameObject.GetComponent<BoxCollider>();
                KnifeCollider.enabled = true;
                collider1 = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && On == true)
            {
                var KnifeCollider = KnifeGameObject.GetComponent<BoxCollider>();
                KnifeCollider.enabled = false;
                KnifeAnimatorController.Play("knife1");
                On = false;
                timer = 0;
            }
        }
        else
        {
            KnifeGameObject.SetActive(false);
        }
    }
}

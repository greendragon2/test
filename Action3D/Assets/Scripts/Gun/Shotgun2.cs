using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shotgun2 : MonoBehaviour
{
    public GameObject slot;
    public GameObject pistol1;
    public float damage;
    public float fireRate;
    public float range;
    public AudioClip shotSFX;
    public AudioSource audioSource;
    public AudioClip shotSFXnot_bullet;
    public AudioClip shotSFX_bullet_add;
    public Transform BulletSpawn;
    public Transform BulletSpawn1;
    public Transform pistol_trnsform;
    public float force;
    private float nextFire = 0;
    public GameObject bullet;
    public GameObject hitEffect;
    public GameObject AmmoInGunText;
    public GameObject AmmoInGunText1;
    public GameObject hitIcon;
    public Ammo_box ammobox;
    public int ammo;
    public int AmmoInGun = 0;
    public GameObject player;

    public bool AddAudio;
    public float time = 0;

    public bool first;
    public bool second;
    public bool trigger;

    public GameObject chargingtext;
    public LayerMask layerMask;
    void Update()
    {
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (slot.GetComponent<Slot>().InSlot == "Shotgun2")
        {
            if (AmmoInGun == 0)
            {
                chargingtext.SetActive(true);
            }
            else
            {
                chargingtext.SetActive(false);
            }

            GetComponent<Player_Controller>().pistol = true;
            pistol1.SetActive(true);
            AmmoInGunText.SetActive(true);

            AmmoInGunText1.GetComponent<Text>().text = AmmoInGun.ToString();


            if (AddAudio == true)
            {
                time += Time.deltaTime;
                if (time >= 3)
                {
                    ammobox.ammoIntShotgun -= 2;
                    AmmoInGun += 2;
                    AddAudio = false;
                    time = 0;
                    first = false;
                    second = false;
                }
            }

            ammo = ammobox.ammoIntShotgun;

            if (Input.GetKey(KeyCode.Mouse0) & Time.time > nextFire)
            {
                if (gameObject.GetComponent<InventoryController>().add_object == false)
                {
                    nextFire = Time.time + 1 / fireRate;
                    Shot();
                }
            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (AmmoInGun <= 0)
                {
                    if (ammo >= 0)
                    {
                        if (ammo >= 2)
                        {
                            audioSource.PlayOneShot(shotSFX_bullet_add);
                            AddAudio = true;
                        }
                    }
                }
            }
        }
        else
        {
            GetComponent<Player_Controller>().pistol = false;
            pistol1.SetActive(false);
            chargingtext.SetActive(false);
        }
    }
    void Shot()
    {
        if (AmmoInGun >= 1)
        {
            AmmoInGun -= 1;
            audioSource.PlayOneShot(shotSFX);
            if (first == false)
            {
                Instantiate(bullet, BulletSpawn.position, BulletSpawn.rotation);
                first = true;
                if (trigger == false)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(BulletSpawn.position, pistol_trnsform.transform.forward, out hit, range, layerMask))
                    {
                        if (hit.rigidbody != null)
                        {
                            hit.rigidbody.AddForce(-hit.normal * force);
                            if (hit.collider.gameObject.tag == "Enemy")
                            {
                                hit.collider.gameObject.GetComponent<Enemy_Controller>().hp -= damage;
                            }
                        }
                        if (hit.collider.gameObject.tag != "Enemy")
                        {
                            if (hit.collider.gameObject.tag != "bullet")
                            {
                                GameObject imact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                                GameObject imact1 = Instantiate(hitIcon, hit.point, Quaternion.LookRotation(hit.normal));
                                Destroy(imact, 5f);
                                imact1.transform.parent = hit.transform;
                                imact.transform.parent = hit.transform;
                            }
                        }
                    }
                }
                else
                {
                    audioSource.PlayOneShot(shotSFXnot_bullet);
                }
            }
            else if (second == false)
            {
                Instantiate(bullet, BulletSpawn1.position, BulletSpawn.rotation);
                first = true;
                if (trigger == false)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(BulletSpawn.position, pistol_trnsform.transform.forward, out hit, range))
                    {
                        if (hit.rigidbody != null)
                        {
                            hit.rigidbody.AddForce(-hit.normal * force);
                            if (hit.collider.gameObject.tag == "Enemy")
                            {
                                hit.collider.gameObject.GetComponent<Enemy_Controller>().hp -= damage;
                            }
                            if (hit.collider.gameObject.tag == "marauder")
                            {
                                hit.collider.gameObject.GetComponent<hpEnemy>().hp -= damage;
                            }
                        }

                        if (hit.collider.gameObject.tag != "Enemy")
                        {
                            if (hit.collider.gameObject.tag != "marauder")
                            {
                                GameObject imact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
                                GameObject imact1 = Instantiate(hitIcon, hit.point, Quaternion.LookRotation(hit.normal));
                                Destroy(imact, 5f);
                            }
                        }
                    }
                }
                else
                {
                    audioSource.PlayOneShot(shotSFXnot_bullet);
                }
            }
        }
        else
        {
            audioSource.PlayOneShot(shotSFXnot_bullet);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AkAnim : MonoBehaviour
{
    public bool aim;
    public Animator AnimatorAk;
    public bool aimTrue = false;
    public GameObject Player;

    void Update()
    {
        if(aim == true)
        {
            if(aimTrue==true)
            {
                AnimatorAk.Play("AkAnim1");
                aimTrue = false;
            }
        }
        if (aim == true)
        {
            if (aimTrue == false)
            {
                AnimatorAk.Play("AkAnim");
                aimTrue = true;
            }
        }
        //AnimatorAk.Play("AkAnim1");

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            aimTrue = true;
            aim = true;
        }
        else
        {
            aim = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Player.GetComponent<Ak>().trigger = true;
        Player.GetComponent<Automatic>().trigger = true;
        Player.GetComponent<Shotgun1>().trigger = true;
        Player.GetComponent<Shotgun2>().trigger = true;
        Player.GetComponent<Shotgun>().trigger = true;
        Player.GetComponent<Submachine>().trigger = true;
        Player.GetComponent<assault1>().trigger = true;
        Player.GetComponent<assault2>().trigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        Player.GetComponent<Ak>().trigger = false;
        Player.GetComponent<Automatic>().trigger = false;
        Player.GetComponent<Shotgun1>().trigger = false;
        Player.GetComponent<Shotgun2>().trigger = false;
        Player.GetComponent<Shotgun>().trigger = true;
        Player.GetComponent<Submachine>().trigger = false;
        Player.GetComponent<assault1>().trigger = false;
        Player.GetComponent<assault2>().trigger = false;
    }
}

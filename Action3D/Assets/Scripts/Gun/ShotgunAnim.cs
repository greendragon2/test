using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunAnim : MonoBehaviour
{
    public bool aim;
    public Animator AnimatorPistol;
    public bool aimTrue = true;
    public GameObject Player;

    void Update()
    {

        if (aim == true)
        {
            if (aimTrue == true)
            {
                AnimatorPistol.Play("shotgun2");
                aimTrue = false;
            }
        }
        if (aim == true)
        {
            if (aimTrue == false)
            {
                AnimatorPistol.Play("shotgun1");
                aimTrue = true;
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            aimTrue = true;
            aim = true;
        }
        else
        {
            aim = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Player.GetComponent<Shotgun2>().trigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        Player.GetComponent<Shotgun2>().trigger = false;
    }
}

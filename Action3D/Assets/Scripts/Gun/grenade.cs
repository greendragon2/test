using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grenade : MonoBehaviour
{
    public GameObject slot;
    public GameObject player;
    public GameObject GrenadeGameObject;
    public GameObject flight;
    public GameObject cube;

    void Update()
    {
        Instantiate(cube, player.transform.up, player.transform.rotation);
        //Instantiate(cube, transform.up, transform.rotation);
        Instantiate(cube, transform.forward, transform.rotation);
        slot = player.GetComponent<selected_slot>().IsActivity;
        if (slot.GetComponent<Slot>().InSlot == "grenade")
        {
            GrenadeGameObject.SetActive(true);
            if (Input.GetKey(KeyCode.Mouse0))
            {
                Instantiate(flight, player.transform.up, player.transform.rotation);
            }
        }
        else
        {
            GrenadeGameObject.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolAnim : MonoBehaviour
{
    public bool aim;
    public Animator AnimatorPistol;
    public bool aimTrue = false;
    public GameObject Player;

    void Update()
    {
        
        if(aimTrue==true & aim == true)
        {
            AnimatorPistol.Play("PistolOne");
            aimTrue = false;
        }
        if (aimTrue == false & aim == true)
        {
            AnimatorPistol.Play("PistolTwo");
            aimTrue = true;
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            aimTrue = true;
            aim = true;
        }
        else
        {
            aim = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Player.GetComponent<Pistol_Controller>().trigger = true;
        Player.GetComponent<Pistol_with_muffler>().trigger = true;
        Player.GetComponent<Revolver_Controller>().trigger = true;
        Player.GetComponent<Uzi>().trigger = true;
    }
    private void OnTriggerExit(Collider other)
    {
        Player.GetComponent<Pistol_Controller>().trigger = false;
        Player.GetComponent<Pistol_with_muffler>().trigger = false;
        Player.GetComponent<Revolver_Controller>().trigger = false;
        Player.GetComponent<Uzi>().trigger = false;
    }
}

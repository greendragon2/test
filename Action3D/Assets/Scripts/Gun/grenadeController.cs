using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class grenadeController : MonoBehaviour
{
    public GameObject slot;
    public GameObject player;
    public GameObject GrenadeGameObject;
    public GameObject flight;
    public GameObject cube;
    public GameObject Smawn;
    public Sprite sprite;
    public GameObject Is_Activity;


    public int count = 1;

    void Update()
    {
        slot = player.GetComponent<selected_slot>().IsActivity;
        Is_Activity = GetComponent<selected_slot>().IsActivity;
        if (count <= 0)
        {
            Is_Activity.GetComponent<Image>().sprite = sprite;
            Is_Activity.GetComponent<Slot>().InSlot = null;
            Is_Activity.GetComponent<Slot>().IsFull = false;
            count = 1;
        }
        if (slot.GetComponent<Slot>().InSlot == "grenade")
        {
            GrenadeGameObject.SetActive(true);
            if (gameObject.GetComponent<InventoryController>().add_object == false)
            {
                if (Input.GetKeyUp(KeyCode.Mouse0))
                {

                    if (count <= 0)
                    {
                        Is_Activity.GetComponent<Image>().sprite = sprite;
                        Is_Activity.GetComponent<Slot>().InSlot = null;
                        Is_Activity.GetComponent<Slot>().IsFull = false;
                        count = 1;
                    }
                    else
                    {
                        Instantiate(flight, Smawn.transform.position, Smawn.transform.rotation);
                    }
                    count--;
                }
            }
        }
        else
        {
            GrenadeGameObject.SetActive(false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public int speed = 30;
    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        Destroy(gameObject, 1f);
    }
    public void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlightWirelessBomb : MonoBehaviour
{
    public int once;
    public float timer;
    public AudioSource AudioSource;
    public bool AudioSourceBool;
    public AudioClip AudioClip1;
    public AudioClip AudioClip2;
    public Object Effect;
    public bool first;
    public bool ColliderBox;
    public bool end;
    public GameObject[] player;
    public bool floor;
    public GameObject bomb;

    private void OnCollisionEnter(Collision collision)
    {
        ColliderBox = true;
        floor = true;
        if (collision.gameObject.tag != "Player")
        {
            AudioSource.PlayOneShot(AudioClip1);
        }
    }

    void Start()
    {
        AudioSource.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioGun");
        player = GameObject.FindGameObjectsWithTag("Player");
        var grenade = gameObject.GetComponent<SphereCollider>();
        grenade.enabled = false;
        
    }
    void Update()
    {
        if (end == true)
        {
            if (timer >= 0.05)
            {
                transform.localScale = new Vector3(0, 0, 0);
            }
        }
        timer += Time.deltaTime;
        if (once !< 120)
        {
            if (ColliderBox != true)
            transform.Translate(Vector3.forward * 25 * Time.deltaTime);
            once += 1;
        }
        if (player[0].GetComponent<wireless_bomb>().boom == true)
        {
            AudioSource.PlayOneShot(AudioClip2);
            var grenade = gameObject.GetComponent<SphereCollider>();
            grenade.enabled = true;
            if (first == true)
            {
                if (floor == true)
                {
                    Instantiate(Effect, transform.position, transform.rotation);
                }
                timer = 0;
                first = false;
                Destroy(bomb, 0.5f);
            }
            end = true; 
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        floor = false;
    }
}
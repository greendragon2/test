using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firstAidKit : MonoBehaviour
{
    public Animator animator;
    public float timer;
    public bool timerbool;
    public bool timerboolSecond;
    public GameObject text;
    public GameObject count;

    public AudioClip openAudio;
    public AudioSource audiosource;

    public bool first = false;
    public void OnTriggerStay(Collider other)
    {
        if (timerboolSecond == true)
        {
            if (timer > 1)
            {
                timerboolSecond = false;
                timerbool = false;
                timer = 0;
                other.gameObject.GetComponent<firstAidKitController>().quantity--;
                gameObject.GetComponent<Player_Controller>().hp = 50;
                if (first == false)
                {
                    first = true;
                    audiosource.PlayOneShot(openAudio);
                    animator.Play("Open");
                }
            }
        }
        if (timerbool == true)
        {
            timer += Time.deltaTime;
        }
        if (other.gameObject.tag == "first aid kit")
        {
            text.SetActive(true);
            count.SetActive(true);
            if (Input.GetKey(KeyCode.H))
            {
                if (gameObject.GetComponent<Player_Controller>().hp < 50)
                {
                    if (other.gameObject.GetComponent<firstAidKitController>().quantity > 0)
                    {
                        timerboolSecond = true;
                        timerbool = true;
                    }
                }
            }
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "first aid kit")
        {
            text.SetActive(false);
            count.SetActive(false);
        }
    }
}
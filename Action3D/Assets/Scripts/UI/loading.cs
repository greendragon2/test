using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loading : MonoBehaviour
{
    public Image loadingImage;
    public Text loadingText;
    public Text text;
    public List<string> clue;

    public void Start()
    {
        clue.Add("To expose health, you need to have more than 50 percent of water and food.");
        clue.Add("Spending food sparingly is not so much and it is a currency.");
        clue.Add("Your chance to survive is to find the bunker.");
        clue.Add("Mutants are not kind.");
        clue.Add("GreenDragonStudio");
        clue.Add("Help other people, they can give you useful things.");
        StartCoroutine(AsyncLoad());
        text.text = clue[Random.Range(0, 5)];
    }

    IEnumerator AsyncLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Test1");
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            loadingImage.fillAmount = progress;
            loadingText.text = string.Format("{0:0}%", progress*100f);
            yield return null;
        }
    }
}

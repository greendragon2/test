using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextEffect : MonoBehaviour
{
    public Text TextGameObject;
    private string text;
    void Start()
    {
        text = TextGameObject.text;
        TextGameObject.text = "";
        StartCoroutine(Textoroutine());
    }
    IEnumerator Textoroutine()
    {
        foreach(char abc in text)
        {
            TextGameObject.text += abc;
            yield return new WaitForSeconds(0.05f);
        }
    }
}

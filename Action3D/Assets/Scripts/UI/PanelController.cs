using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public GameObject PanelOne;
    public GameObject PanelTwo;
    public GameObject PanelThree;
    public GameObject PanelFour;
    public GameObject PanelFive;
    void Start()
    {
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 1)
        {
            PanelOne.SetActive(true);
            PanelTwo.SetActive(false);
            PanelThree.SetActive(false);
            PanelFour.SetActive(false);
            PanelFive.SetActive(false);
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 2)
        {
            PanelOne.SetActive(false);
            PanelTwo.SetActive(true);
            PanelThree.SetActive(false);
            PanelFour.SetActive(false);
            PanelFive.SetActive(false);
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 3)
        {
            PanelOne.SetActive(false);
            PanelTwo.SetActive(false);
            PanelThree.SetActive(true);
            PanelFour.SetActive(false);
            PanelFive.SetActive(false);
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 4)
        {
            PanelOne.SetActive(false);
            PanelTwo.SetActive(false);
            PanelThree.SetActive(false);
            PanelFour.SetActive(true);
            PanelFive.SetActive(false);
        }
        if (PlayerPrefs.GetFloat("AdditionalInventorySize") == 5)
        {
            PanelOne.SetActive(false);
            PanelTwo.SetActive(false);
            PanelThree.SetActive(false);
            PanelFour.SetActive(false);
            PanelFive.SetActive(true);
        }
    }
}

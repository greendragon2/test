using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectButtonText : MonoBehaviour
{
    public GameObject button1;
    public GameObject Button2;
    public float alpha;
    public float timer;
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > 0.1)
        {
            button();
            button2();
            timer = 0;
        }
    }
    public void button2()
    {
        if (alpha < 1)
        {
            Color colorbutton2 = Button2.GetComponent<Text>().color;
            colorbutton2.a = alpha;
            Button2.GetComponent<Text>().color = colorbutton2;
        }
    }
    public void button()
    {
        if (alpha < 1)
        {
            alpha += 0.03f;
            Color color = button1.GetComponent<Text>().color;
            color.a = alpha;
            button1.GetComponent<Text>().color = color;    
        }
    }
}

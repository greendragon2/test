using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEnemy : MonoBehaviour
{
    public GameObject[] Enemy;
    public GameObject[] marauder;

    void Update()
    {
        Enemy = GameObject.FindGameObjectsWithTag("Enemy");
        for(int i = 0; i < Enemy.Length; i++)
        {
            Enemy[i].GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioEnemy");
        }
        marauder = GameObject.FindGameObjectsWithTag("marauder");
        for (int i = 0; i < marauder.Length; i++)
        {
            marauder[i].GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioEnemy");
        }
    }
}
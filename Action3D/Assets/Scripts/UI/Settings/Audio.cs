using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Audio : MonoBehaviour
{
    public Sprite Image1;
    public Sprite Image2;
    public Sprite Image3;
    public Sprite Image4;

    public Image GameObjectImage;
    public GameObject GameObjectSlider;

    public int type;

    private void Start()
    {
        if (type == 1)
        {
            GameObjectSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("AudioGun");
        }
        else if (type == 2)
        {
            GameObjectSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("AudioPlayer");
        }
        else if (type == 3)
        {
            GameObjectSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("AudioEnemy");
        }
        else if (type == 4)
        {
            GameObjectSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("AudioMusic");
        }
    }

    private void Update()
    {
        if (GameObjectSlider.GetComponent<Slider>().value <= 0.1)
        {
            GameObjectImage.sprite = Image1;
        }
        if (GameObjectSlider.GetComponent<Slider>().value > 0.1 && GameObjectSlider.GetComponent<Slider>().value < 0.4)
        {
            GameObjectImage.sprite = Image4;
        }
        if (GameObjectSlider.GetComponent<Slider>().value < 0.8 && GameObjectSlider.GetComponent<Slider>().value > 0.4)
        {
            GameObjectImage.sprite = Image2;
        }
        if (GameObjectSlider.GetComponent<Slider>().value >= 0.8)
        {
            GameObjectImage.sprite = Image3;
        }
    }
}

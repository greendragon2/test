using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsApply : MonoBehaviour
{
    public int fullScreenInt;
    public GameObject FullScreen;
    public GameObject FullScreenScript;

    public GameObject AdditionalInventorySize;
    public float AdditionalInventorySizeInt;

    public GameObject AudioGun;
    public float AudioGunInt;

    public GameObject AudioPlayer;
    public float AudioPlayerInt;

    public GameObject AudioEnemy;
    public float AudioEnemyInt;

    public GameObject AudioMusic;
    public float AudioMusicInt;
    public void Update()
    {
        AdditionalInventorySizeInt = AdditionalInventorySize.GetComponent<Slider>().value;
        AudioGunInt = AudioGun.GetComponent<Slider>().value;
        AudioPlayerInt = AudioPlayer.GetComponent<Slider>().value;
        AudioEnemyInt = AudioEnemy.GetComponent<Slider>().value;
        AudioMusicInt = AudioMusic.GetComponent<Slider>().value;
    }
    public void ApplySettings()
    {
        if (FullScreen.GetComponent<Toggle>().isOn == true)
        {
            fullScreenInt = 1;
        }
        if (FullScreen.GetComponent<Toggle>().isOn == false)
        {
            fullScreenInt = 0;
        }
        PlayerPrefs.DeleteKey("fullScreen");
        PlayerPrefs.SetInt("fullScreen", fullScreenInt);

        
        PlayerPrefs.DeleteKey("AdditionalInventorySize");
        PlayerPrefs.SetFloat("AdditionalInventorySize", AdditionalInventorySizeInt);
        
        PlayerPrefs.DeleteKey("AudioGun");
        PlayerPrefs.SetFloat("AudioGun", AudioGunInt);

        PlayerPrefs.DeleteKey("AudioPlayer");
        PlayerPrefs.SetFloat("AudioPlayer", AudioPlayerInt);

        PlayerPrefs.DeleteKey("AudioEnemy");
        PlayerPrefs.SetFloat("AudioEnemy", AudioEnemyInt);

        PlayerPrefs.DeleteKey("AudioMusic");
        PlayerPrefs.SetFloat("AudioMusic", AudioMusicInt);

        PlayerPrefs.Save();
    }
}

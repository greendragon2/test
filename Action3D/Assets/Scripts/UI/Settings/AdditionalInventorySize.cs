using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdditionalInventorySize : MonoBehaviour
{
    public GameObject textObject;
    public float slider;
    public void Start()
    {
        gameObject.GetComponent<Slider>().value = PlayerPrefs.GetFloat("AdditionalInventorySize");
    }
    void Update()
    {
        slider = gameObject.GetComponent<Slider>().value;
        textObject.GetComponent<Text>().text = slider.ToString();
    }
}

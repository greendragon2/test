using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullScreen : MonoBehaviour
{
    public void Start()
    {
        if (PlayerPrefs.GetInt("fullScreen") == 1)
        {
            gameObject.GetComponent<Toggle>().isOn = true;
        }
        else if (PlayerPrefs.GetInt("fullScreen") == 0)
        {
            gameObject.GetComponent<Toggle>().isOn = false;
        }
    }
    public void Update()
    {
        if (gameObject.GetComponent<Toggle>().isOn == true)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = false;
        }
    }
}

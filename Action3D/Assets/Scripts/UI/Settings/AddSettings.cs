using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSettings : MonoBehaviour
{
    public GameObject[] gun;
    public int i;
    public GameObject player;
    private void Start()
    {
        while (i < 15)
        {
            gun[i].GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioGun");
            i++;
        }
        player.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioPlayer");
    }
}

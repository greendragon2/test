using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pause : MonoBehaviour
{
    public GameObject pause1;
    public GameObject pause2;
    public GameObject Player;

    public bool ESC;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) & ESC == false)
        {
            Player.GetComponent<selected_slot>().pause = true;
            pause1.SetActive(true);
            pause2.SetActive(false);
            Time.timeScale = 0;
            Screen.lockCursor = false;
            var PlayerController = Player.GetComponent<Player_Controller>();
            PlayerController.enabled = false;
            var Pistol = Player.GetComponent<Pistol_Controller>();
            Pistol.enabled = false;
            var Ak = Player.GetComponent<Ak>();
            Ak.enabled = false;
            var InventoryController = Player.GetComponent<InventoryController>();
            InventoryController.enabled = false;
            var Flashlight = gameObject.GetComponent<Flashlight>();
            Flashlight.enabled = false;
            var Revolver = Player.GetComponent<Revolver_Controller>();
            Revolver.enabled = false;
            var Automatic = Player.GetComponent<Automatic>();
            Automatic.enabled = false;
            var pistol = Player.GetComponent<Pistol_with_muffler>();
            pistol.enabled = false;
            var pistol1 = Player.GetComponent<Shotgun>();
            pistol1.enabled = false;
            var pistol2 = Player.GetComponent<Shotgun1>();
            pistol2.enabled = false;
            var pistol3 = Player.GetComponent<Shotgun2>();
            pistol3.enabled = false;
            var pistol4 = Player.GetComponent<grenadeController>();
            pistol4.enabled = false;
            var pistol5 = Player.GetComponent<Uzi>();
            pistol5.enabled = false;
            var pistol6 = Player.GetComponent<Submachine>();
            pistol6.enabled = false;
            var pistol7 = Player.GetComponent<assault1>();
            pistol7.enabled = false;
            var pistol8 = Player.GetComponent<assault2>();
            pistol8.enabled = false;
            var pistol9 = Player.GetComponent<bottleWater>();
            pistol9.enabled = false;
            var pistol10 = Player.GetComponent<food>();
            pistol10.enabled = false;
            var Ammo_box = Player.GetComponent<Ammo_box>();
            Ammo_box.enabled = false;
            var wireless_bomb = Player.GetComponent<wireless_bomb>();
            wireless_bomb.enabled = false;

            ESC = true;
        }
        else if (Input.GetKeyUp(KeyCode.Escape) & ESC == true)
        {
            pause1.SetActive(false);
            pause2.SetActive(true);
            Time.timeScale = 1;
            Screen.lockCursor = true;
            Player.GetComponent<selected_slot>().pause = false;
            var PlayerController = Player.GetComponent<Player_Controller>();
            PlayerController.enabled = true;
            var Pistol = Player.GetComponent<Pistol_Controller>();
            Pistol.enabled = true;
            var Ak = Player.GetComponent<Ak>();
            Ak.enabled = true;
            var InventoryController = Player.GetComponent<InventoryController>();
            InventoryController.enabled = true;
            var Flashlight = gameObject.GetComponent<Flashlight>();
            Flashlight.enabled = true;
            var Revolver = Player.GetComponent<Revolver_Controller>();
            Revolver.enabled = true;
            var Automatic = Player.GetComponent<Automatic>();
            Automatic.enabled = true;

            var pistol = Player.GetComponent<Pistol_with_muffler>();
            pistol.enabled = true;
            var pistol1 = Player.GetComponent<Shotgun>();
            pistol1.enabled = true;
            var pistol2 = Player.GetComponent<Shotgun1>();
            pistol2.enabled = true;
            var pistol3 = Player.GetComponent<Shotgun2>();
            pistol3.enabled = true;
            var pistol4 = Player.GetComponent<grenadeController>();
            pistol4.enabled = true;
            var pistol5 = Player.GetComponent<Uzi>();
            pistol5.enabled = true;
            var pistol6 = Player.GetComponent<Submachine>();
            pistol6.enabled = true;
            var pistol7 = Player.GetComponent<assault1>();
            pistol7.enabled = true;
            var pistol8 = Player.GetComponent<assault2>();
            pistol8.enabled = true;
            var pistol9 = Player.GetComponent<bottleWater>();
            pistol9.enabled = true;
            var pistol10 = Player.GetComponent<food>();
            pistol10.enabled = true;
            var Ammo_box = Player.GetComponent<Ammo_box>();
            Ammo_box.enabled = true;
            var wireless_bomb = Player.GetComponent<wireless_bomb>();
            wireless_bomb.enabled = true;

            ESC = false;
        }
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void Mune()
    {
        //var Inventory = Player.GetComponent<selected_slot>();
        //Inventory.enabled = true;
        var PlayerController = Player.GetComponent<Player_Controller>();
        PlayerController.enabled = true;
        var Pistol = Player.GetComponent<Pistol_Controller>();
        Pistol.enabled = true;
        var Ak = Player.GetComponent<Ak>();
        Ak.enabled = true;
        var InventoryController = Player.GetComponent<InventoryController>();
        InventoryController.enabled = true;
        var Flashlight = gameObject.GetComponent<Flashlight>();
        Flashlight.enabled = true;
        var Revolver = Player.GetComponent<Revolver_Controller>();
        Revolver.enabled = true;
        var Automatic = Player.GetComponent<Automatic>();
        Automatic.enabled = true;
        Player.GetComponent<selected_slot>().pause = true;
        var pistol = Player.GetComponent<Pistol_with_muffler>();
        pistol.enabled = true;
        var pistol1 = Player.GetComponent<Shotgun>();
        pistol1.enabled = true;
        var pistol2 = Player.GetComponent<Shotgun1>();
        pistol2.enabled = true;
        var pistol3 = Player.GetComponent<Shotgun2>();
        pistol3.enabled = true;
        var pistol4 = Player.GetComponent<grenadeController>();
        pistol4.enabled = true;
        var pistol5 = Player.GetComponent<Uzi>();
        pistol5.enabled = true;
        var pistol6 = Player.GetComponent<Submachine>();
        pistol6.enabled = true;
        var pistol7 = Player.GetComponent<assault1>();
        pistol7.enabled = true;
        var pistol8 = Player.GetComponent<assault2>();
        pistol8.enabled = true;
        var pistol9 = Player.GetComponent<bottleWater>();
        pistol9.enabled = true;
        var pistol10 = Player.GetComponent<food>();
        pistol10.enabled = true;
        var Ammo_box = Player.GetComponent<Ammo_box>();
        Ammo_box.enabled = true;
        var wireless_bomb = Player.GetComponent<wireless_bomb>();
        wireless_bomb.enabled = true;
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunIsActivityFix : MonoBehaviour
{
    void Update()
    {
        if(gameObject.GetComponent<selected_slot>().IsActivity.GetComponent<Slot>().InSlot == "pistol")
        {
            print("1");
            var Ak = gameObject.GetComponent<Ak>();
            Ak.enabled = false; 
            var Revolver = gameObject.GetComponent<Revolver_Controller>();
            Revolver.enabled = false;
            var Automatic = gameObject.GetComponent<Automatic>();
            Automatic.enabled = false;
            var pistol = gameObject.GetComponent<Pistol_with_muffler>();
            pistol.enabled = false;
            var pistol1 = gameObject.GetComponent<Shotgun>();
            pistol1.enabled = false;
            var pistol2 = gameObject.GetComponent<Shotgun1>();
            pistol2.enabled = false;
            var pistol3 = gameObject.GetComponent<Shotgun2>();
            pistol3.enabled = false;
            var pistol5 = gameObject.GetComponent<Uzi>();
            pistol5.enabled = false;
            var pistol6 = gameObject.GetComponent<Submachine>();
            pistol6.enabled = false;
            var pistol7 = gameObject.GetComponent<assault1>();
            pistol7.enabled = false;
            var pistol8 = gameObject.GetComponent<assault2>();
            pistol8.enabled = false;
        }
        if (gameObject.GetComponent<selected_slot>().IsActivity.GetComponent<Slot>().InSlot == "Ak")
        {
            print("2");
            var Pistol = gameObject.GetComponent<Pistol_Controller>();
            Pistol.enabled = false;
            var Revolver = gameObject.GetComponent<Revolver_Controller>();
            Revolver.enabled = false;
            var Automatic = gameObject.GetComponent<Automatic>();
            Automatic.enabled = false;
            var pistol = gameObject.GetComponent<Pistol_with_muffler>();
            pistol.enabled = false;
            var pistol1 = gameObject.GetComponent<Shotgun>();
            pistol1.enabled = false;
            var pistol2 = gameObject.GetComponent<Shotgun1>();
            pistol2.enabled = false;
            var pistol3 = gameObject.GetComponent<Shotgun2>();
            pistol3.enabled = false;
            var pistol5 = gameObject.GetComponent<Uzi>();
            pistol5.enabled = false;
            var pistol6 = gameObject.GetComponent<Submachine>();
            pistol6.enabled = false;
            var pistol7 = gameObject.GetComponent<assault1>();
            pistol7.enabled = false;
            var pistol8 = gameObject.GetComponent<assault2>();
            pistol8.enabled = false;
        }
    }
}

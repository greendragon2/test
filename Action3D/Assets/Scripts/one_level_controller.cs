using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class one_level_controller : MonoBehaviour
{
    public GameObject LampOne;
    public GameObject LampTwo;
    public GameObject LampThree;
    public float time;

    void Start()
    {
        SceneController.namber_scene = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time <= 5)
        {
            LampOne.SetActive(false);
            LampTwo.SetActive(false);
        }
        else if (time <= 6)
        {
            LampOne.SetActive(true);
            LampTwo.SetActive(true);
        }
        else if(time <= 7)
        {
            LampThree.SetActive(false);
        }
        else if(time <= 8)
        {
            LampThree.SetActive(true);
        }
        else if (time <= 10)
        {
            time = 0;
        }
    }
}

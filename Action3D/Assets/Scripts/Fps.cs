using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fps : MonoBehaviour
{
    public static float fps;
    public GameObject text;
    public bool On;
    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.F) & On == true)
        {
            text.SetActive(true);
            On = false;
        }
        else if (Input.GetKeyUp(KeyCode.F) & On == false)
        {
            text.SetActive(false);
            On = true;
        }
        fps = 1.0f / Time.deltaTime;
        text.GetComponent<Text>().text = ("FPS: " + (int)fps).ToString();
    }
}

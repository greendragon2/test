using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWalk : MonoBehaviour
{
    public float intensity;
    public float amplitude;

    public Vector3 nextSwayVector;
    public Vector3 nextSwayPosition;
    public Vector3 startLocalPosition;

    public GameObject player;

    void Start()
    {
        nextSwayVector = Vector3.up * amplitude;
        nextSwayPosition = transform.localPosition + nextSwayVector;
        startLocalPosition = transform.localPosition;
    }

    void Update()
    {
        if (player.GetComponent<Player_Controller>().runBool == true && player.GetComponent<Player_Controller>().WolkBool == false)
        {
            GetComponent<Camera>().enabled = true;
            GetComponent<CameraWalk>().enabled = false;
        }
        if (player.GetComponent<Player_Controller>().WalkBool == true && player.GetComponent<Player_Controller>().runBool == false)
        {
            GetComponent<Camera>().enabled = false;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, nextSwayPosition, intensity * Time.deltaTime);

            if (Vector3.SqrMagnitude(transform.localPosition - nextSwayPosition) < 0.01f)
            {
                nextSwayVector = -nextSwayVector;

                nextSwayPosition = startLocalPosition + nextSwayVector;
            }
        }
        else
        {
            GetComponent<Camera>().enabled = true;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, startLocalPosition, intensity * Time.deltaTime);
        }
    }
}
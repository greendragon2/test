using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dia : MonoBehaviour
{
    public GameObject Player;
    public GameObject Bars;
    public GameObject Controller;
    public GameObject Canvas;
    public Object PlayerDia;
    void Update()
    {
        if(Player.GetComponent<Player_Controller>().hp <= 0)
        {
            Screen.lockCursor = false;
            Instantiate(PlayerDia, Player.transform.position, Player.transform.rotation);
            Player.transform.localScale = new Vector3(0, 0, 0);
            Destroy(gameObject);
            Bars.SetActive(false);
            Controller.SetActive(false);
            Canvas.SetActive(true);

            var PlayerController = Player.GetComponent<Player_Controller>();
            PlayerController.enabled = false;
            var Pistol = Player.GetComponent<Pistol_Controller>();
            Pistol.enabled = false;
            var Ak = Player.GetComponent<Ak>();
            Ak.enabled = false;
            var InventoryController = Player.GetComponent<InventoryController>();
            InventoryController.enabled = false;
            var Revolver = Player.GetComponent<Revolver_Controller>();
            Revolver.enabled = false;
            var Automatic = Player.GetComponent<Automatic>();
            Automatic.enabled = false;
            var pistol = Player.GetComponent<Pistol_with_muffler>();
            pistol.enabled = false;
            var pistol1 = Player.GetComponent<Shotgun>();
            pistol1.enabled = false;
            var pistol2 = Player.GetComponent<Shotgun1>();
            pistol2.enabled = false;
            var pistol3 = Player.GetComponent<Shotgun2>();
            pistol3.enabled = false;
            var pistol4 = Player.GetComponent<grenadeController>();
            pistol4.enabled = false;
            var pistol5 = Player.GetComponent<Uzi>();
            pistol5.enabled = false;
            var pistol6 = Player.GetComponent<Submachine>();
            pistol6.enabled = false;
            var pistol7 = Player.GetComponent<assault1>();
            pistol7.enabled = false;
            var pistol8 = Player.GetComponent<assault2>();
            pistol8.enabled = false;
            var pistol9 = Player.GetComponent<bottleWater>();
            pistol9.enabled = false;
            var pistol10 = Player.GetComponent<food>();
            pistol10.enabled = false;
        }
    }
}
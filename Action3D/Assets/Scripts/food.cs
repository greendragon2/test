using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class food : MonoBehaviour
{
    public GameObject slot;
    public GameObject foodGameObject;
    public Sprite sprite;
    public bool InSlot = true;
    public int count = 1;
    void Update()
    {
        slot = GetComponent<selected_slot>().IsActivity;
        if (count <= 0)
        {
            slot.GetComponent<Image>().sprite = sprite;
            slot.GetComponent<Slot>().InSlot = null;
            slot.GetComponent<Slot>().IsFull = false;
            count = 1;
        }
        if (slot.GetComponent<Slot>().InSlot == "food")
        {
            if (gameObject.GetComponent<InventoryController>().add_object == false)
            {
                InSlot = true;
                foodGameObject.SetActive(true);
                if (Input.GetKeyUp(KeyCode.Mouse0))
                {
                    GetComponent<Player_Controller>().hungerInt = 100;
                }
            }
        }
        else
        {
            InSlot = false;
            foodGameObject.GetComponent<AnimFood>().timer = 0;
            foodGameObject.GetComponent<AnimFood>().timerBool = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class firstAidKitController : MonoBehaviour
{
    public Text text;
    public int quantity = 6;
    public void Update()
    {
        text.text = quantity.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject open_door;
    public static int namber_scene;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "entrance1")
        {
            open_door.SetActive(true);
            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene(namber_scene);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        open_door.SetActive(false);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

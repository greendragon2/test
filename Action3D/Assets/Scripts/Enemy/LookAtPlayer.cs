using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    public Transform player;
    public GameObject Enemy;
    public bool Enemy1;
    public bool gun;

    private void Start()
    {
        if (gun == true)
        {
            gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("AudioGun");
        }
    }

    public void LateUpdate()
    {
        transform.LookAt(player.transform.position);
    }
    private void OnTriggerStay(Collider other)
    {
        if (Enemy1 == false)
        {
            if (other.gameObject.tag != "Player")
            {
                Enemy.GetComponent<EnemyShot>().fire = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (Enemy1 == false)
        {
            Enemy.GetComponent<EnemyShot>().fire = true;
        }
    }
}

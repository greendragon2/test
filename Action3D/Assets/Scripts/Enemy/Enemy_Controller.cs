using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Controller : MonoBehaviour
{
    public float hp = 50f;
    public GameObject target;
    public float distance;
    public NavMeshAgent NavMeshAgentEnemy;
    public Animator animator;
    public GameObject die;
    public bool diePlayer;

    public AudioSource AudioSource;
    public AudioClip run;
    public AudioClip idle;
    public AudioClip attack;
    public AudioClip idle1;
    public bool mutant;
    public bool mutantType;

    public float WolkTimer;
    public float PunchTimer;
    public float IdleTimer;
    public float fireTimer;

    public void Start()
    {
        animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();
        NavMeshAgentEnemy = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        fireTimer += Time.deltaTime;
        WolkTimer += Time.deltaTime;
        PunchTimer += Time.deltaTime;
        IdleTimer += Time.deltaTime;
        if (diePlayer == false)
        {
            if (target.GetComponent<Player_Controller>().hp < 0.01f)
            {
                Destroy(gameObject, 5f);
                target = transform.parent.gameObject;
                diePlayer = true;
            }
        }
        distance = Vector3.Distance(transform.position, target.transform.position);
        if (mutantType == false)
        {
            if (distance >= 20)
            {
                NavMeshAgentEnemy.enabled = false;
                animator.SetBool("idle", true);
                animator.SetBool("run", false);
                animator.SetBool("Attack", false);
                idleVoid();
            }
            if (distance < 20 && distance > 1.1f)
            {
                NavMeshAgentEnemy.enabled = true;
                NavMeshAgentEnemy.SetDestination(target.transform.position);
                animator.SetBool("idle", false);
                animator.SetBool("run", true);
                animator.SetBool("Attack", false);
                Walk();
            }
            if (distance < 1.1f)
            {
                NavMeshAgentEnemy.enabled = false;
                animator.SetBool("idle", false);
                animator.SetBool("run", false);
                animator.SetBool("Attack", true);
                Punch();
            }
        }
        else
        {
            if (distance >= 20)
            {
                NavMeshAgentEnemy.enabled = false;
                animator.SetBool("idle", true);
                animator.SetBool("run", false);
                animator.SetBool("Attack", false);
                idleVoid();
            }
            if (distance < 20 && distance > 1.5f)
            {
                NavMeshAgentEnemy.enabled = true;
                NavMeshAgentEnemy.SetDestination(target.transform.position);
                animator.SetBool("idle", false);
                animator.SetBool("run", true);
                animator.SetBool("Attack", false);
                Walk();
            }
            if (distance < 1.5f)
            {
                NavMeshAgentEnemy.enabled = false;
                animator.SetBool("idle", false);
                animator.SetBool("run", false);
                animator.SetBool("Attack", true);
                Punch();
            }
        }
        if (hp <= 0.1)
        {
            Instantiate(die, gameObject.transform.position, gameObject.transform.rotation);
            Destroy(gameObject);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Knife")
        {
            hp = hp - 10;
        }
        if (other.gameObject.tag == "grenade")
        {
            hp = hp - 25;
        }
    }
    public void Walk()
    {
        if (mutant == false)
        {
            if (WolkTimer > 0.4f)
            {
                WolkTimer = 0;
                AudioSource.PlayOneShot(run);
            }
        }
        else
        {
            if (WolkTimer > 0.6f)
            {
                WolkTimer = 0;
                AudioSource.PlayOneShot(run);
            }
        }
    }
    public void Punch()
    {
        if (mutant == false)
        {
            if (PunchTimer > 0.6f)
            {
                PunchTimer = 0;
                AudioSource.PlayOneShot(attack);
            }
        }
        else
        {
            if (PunchTimer > 0.7f)
            {
                PunchTimer = 0;
                AudioSource.PlayOneShot(attack);
            }
        }
    }
    public void idleVoid()
    {
        if (mutant == false)
        {
            if (IdleTimer > 2.5f)
            {
                IdleTimer = 0;
                AudioSource.PlayOneShot(idle);
            }
        }
        else
        {
            if (IdleTimer > 2.9f)
            {
                IdleTimer = 0;
                AudioSource.PlayOneShot(idle1);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "fire")
        {
            if (fireTimer >= 0.5f)
            {
                hp -= 10;
                fireTimer = 0;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hpEnemy : MonoBehaviour
{
    public float hp = 50f;
    public GameObject die;
    public GameObject Enemy;
    public GameObject ak;
    public GameObject target;
    public float distance;
    public float fireTimer;
    
    private void Update()
    {
        fireTimer += Time.deltaTime;
        distance = Vector3.Distance(transform.position, target.transform.position);
        if (hp < 0.1f)
        {
            Instantiate(die, gameObject.transform.position, gameObject.transform.rotation);
            GameObject item = Instantiate(ak, gameObject.transform.position, gameObject.transform.rotation);
            item.AddComponent<maradurAk>();
            item.GetComponent<maradurAk>().AmmoInGun = Enemy.GetComponent<EnemyShot>().AmmoInGun;
            Destroy(Enemy);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "fire")
        {
            if (fireTimer >= 0.5f)
            {
                hp -= 10;
                fireTimer = 0;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "grenade")
        {
            hp = hp - 25;
        }
    }
}

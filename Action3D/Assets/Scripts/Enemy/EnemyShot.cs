using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShot : MonoBehaviour
{
    public float distance;
    public GameObject target;
    public bool targetBool = false;
    public GameObject pistol_trnsform;
    public Transform BulletSpawn;
    public GameObject bullet;
    public float range;
    public GameObject model;
    private float nextFire = 0;
    public float fireRate;
    public AudioSource pistolAudio;
    public AudioClip shot;
    public Animator animator;
    public NavMeshAgent NavMeshAgentEnemy;
    public bool WalkBool;
    public float WalkTime;
    public AudioSource AudioSource;
    public AudioClip run;

    public int AmmoInGun = 30;
    public int charge = 3;
    public float TimerCharge;
    public bool BoolTimerCharge;

    public bool walk;
    public bool fire;

    private void Start()
    {
        animator = model.GetComponent<Animator>();
        NavMeshAgentEnemy = model.GetComponent<NavMeshAgent>();
        AudioSource = model.GetComponent<AudioSource>();
    }
    private void Update()
    {
        if (BoolTimerCharge == true)
        {
            TimerCharge += Time.deltaTime;
        }
        if (targetBool == false)
        {
            if (target.GetComponent<Player_Controller>().hp < 0.1)
            {
                target = GameObject.Find("Spawn");
                targetBool = true;
                Destroy(gameObject, 10f);
            }
        }
        if(WalkBool == true)
        {
            WalkTime += Time.deltaTime;
        }
        if (walk == true)
        {
            if (distance > 45)
            {
                NavMeshAgentEnemy.enabled = false;
                animator.SetBool("idle", true);
                animator.SetBool("run", false);
            }
            if (distance < 45)
            {
                NavMeshAgentEnemy.enabled = true;
                NavMeshAgentEnemy.SetDestination(target.transform.position);
                animator.SetBool("idle", false);
                animator.SetBool("run", true);
                WalkBool = true;
                if (WalkTime >= 1)
                {
                    WalkTime = 0;
                    AudioSource.PlayOneShot(run);
                }
            }
        }

        distance = model.GetComponent<hpEnemy>().distance;
        if (distance <= 25)
        {
            var Look = model.GetComponent<LookAtPlayer>();
            Look.enabled = true;
            if (fire == true)
            {
                RaycastHit hit;
                if (Physics.Raycast(BulletSpawn.position, pistol_trnsform.transform.forward, out hit, range))
                {
                    if (hit.rigidbody != null)
                    {
                        if (hit.collider.gameObject.tag == "Player")
                        {
                            if (Time.time > nextFire)
                            {
                                if (AmmoInGun > 0)
                                {
                                    Instantiate(bullet, BulletSpawn.position, BulletSpawn.rotation);
                                    nextFire = Time.time + 1 / fireRate;
                                    pistolAudio.PlayOneShot(shot);
                                    hit.collider.gameObject.GetComponent<Player_Controller>().hp -= 2f;
                                    AmmoInGun--;
                                }
                                else
                                {
                                    if (charge > 0)
                                    {
                                        BoolTimerCharge = true;
                                    }
                                    if (TimerCharge > 2)
                                    {
                                        BoolTimerCharge = false;
                                        AmmoInGun = 30;
                                        TimerCharge = 0;
                                        charge--;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (distance >= 26)
        {
            var Look = model.GetComponent<LookAtPlayer>();
            Look.enabled = false;
        }
    }
}

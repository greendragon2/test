using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{

    public GameObject camera_player;
    public GameObject camera_player1;

    public Animator player_animator;

    Quaternion StartingRotation;

    public float vertical;
    public float horizontal;
    public float jump;
    public float RotationHorizontal;
    public float RotationVertical;

    public bool isGround;

    public float SpeedWalk = 4;
    public float SpeedRun = 7;
    public float JumpSpeed = 8000;
    public float senssensitivity = 5;
    public bool pistol;

    public AudioSource PlayerAudioSource;
    public AudioClip run;
    public AudioClip walk;
    public AudioClip jump_audio;

    public bool WalkBool;
    public float WalkTime;
    public bool first = false;
    public float hp;

    public Image hpbar;
    public Text hpText;
    public float hp1;

    public Image water;
    public float waterInt = 100f;

    public Image hunger;
    public float hungerInt = 100f;

    public Image fatigue;
    public float fatigueInt = 100f;

    public bool firstjump;

    public float timer;
    public float timer1;
    public float timer_fatigue;
    public bool IsWolk;

    public float timer_water;
    public float timer_hunger;
    public float fatigueIntTimer;
    public float hpTimer;

    public float TimeEnemy;

    public bool runBool;
    public bool WolkBool;
    public bool JumpSpeedBool;

    private float x;
    private float z;

    public float fireTimmer;

    void Start()
    {
        Screen.lockCursor = true;
        player_animator = GetComponent<Animator>();
        StartingRotation = transform.rotation;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "grenade")
        {
            GetDamageGrenade();
        }
    }
    public void OnCollisionStay(Collision collision)
    {
        JumpSpeedBool = false;
        if (collision.gameObject.tag == "Ground")
        {
            isGround = true;
        }
        if (collision.gameObject.tag == "Ammo_box")
        {
            isGround = true;
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "material")
        {
            isGround = true;
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "item")
        {
            isGround = true;
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "first aid kit")
        {
            isGround = true;
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "stone")
        {
            isGround = true;
            JumpSpeedBool = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        isGround = false;
        if (collision.gameObject.tag == "Ammo_box")
        {
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "first aid kit")
        {
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "item")
        {
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "material")
        {
            JumpSpeedBool = true;
        }
        if (collision.gameObject.tag == "stone")
        {
            JumpSpeedBool = true;
        }
    }

    void Update()
    {
        fireTimmer += Time.deltaTime;
        if (runBool == true)
        {
            IsWolk = false;
        }
        x = gameObject.transform.position.x;
        z = gameObject.transform.position.z;
        TimeEnemy += Time.deltaTime;
        fatigueIntTimer += Time.deltaTime;
        fatigue.fillAmount = fatigueInt * 0.01f;
        water.fillAmount = waterInt * 0.01f;
        hunger.fillAmount = hungerInt * 0.01f;

        if (gameObject.transform.position.y > 0.7)
        {
            //gameObject.transform.position = new Vector3(x, -0.78f, z);
        }

        if (WalkBool == true)
        {
            WalkTime += Time.deltaTime;
        }

        RotationHorizontal += Input.GetAxis("Mouse X") * senssensitivity;
        RotationVertical += Input.GetAxis("Mouse Y") * senssensitivity;

        RotationVertical = Mathf.Clamp(RotationVertical, -60, 60);

        Quaternion RotY = Quaternion.AngleAxis(RotationHorizontal, Vector3.up);
        Quaternion RotX = Quaternion.AngleAxis(-RotationVertical, Vector3.right);

        camera_player.transform.rotation = StartingRotation * transform.rotation * RotX;
        transform.rotation = StartingRotation * RotY;

        camera_player1.transform.rotation = StartingRotation * transform.rotation * RotX;
        transform.rotation = StartingRotation * RotY;

        if (isGround == true)
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                if (fatigueInt > 0)
                {
                    if (fatigueIntTimer > 0.1)
                    {
                        if (Input.GetKeyUp(KeyCode.W))
                        {
                            //runBool = true;
                        }
                        fatigueInt -= 0.2f;
                        vertical = Input.GetAxis("Vertical") * Time.deltaTime * SpeedRun;
                        fatigueIntTimer = 0;
                    }
                }
            }
            
            else
            {
                //runBool = false;
                vertical = Input.GetAxis("Vertical") * Time.deltaTime * SpeedWalk;
            }
            horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * SpeedWalk;

            if (fatigueInt < 0)
            {
                vertical = Input.GetAxis("Vertical") * Time.deltaTime * SpeedWalk;
            }
            if (fatigueInt > 0)
            {
                if (JumpSpeedBool == true)
                {
                    jump = Input.GetAxis("Jump") * Time.deltaTime * 1500;
                }
                else
                {
                    jump = Input.GetAxis("Jump") * Time.deltaTime * JumpSpeed;
                }
            }
            //if (fatigueInt > 0)
            //{
                //if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
                //{
                //    player_animator.Play("Jump");
                //}
            //}

                if (Input.GetKeyDown(KeyCode.Space))
            {
                if (fatigueInt > 0)
                {
                    fatigueInt -= 2.5f;
                    player_animator.Play("Jump");
                    JumpAudio();
                }
            }
            if (vertical > 0)
            {
                if (Input.GetKey(KeyCode.LeftControl))
                {
                    if (fatigueInt > 0)
                    {
                        player_animator.Play("Run");
                        RunAudio();
                        //runBool = true;
                    }
                }
                else
                {
                    player_animator.Play("Walk");
                    WalkAudio();
                    IsWolk = true;
                }
            }
            else if (vertical < 0)
            {
                WalkAudio();
                player_animator.Play("WalkBack");
                IsWolk = true;
            }
            else if (horizontal > 0)
            {
                WalkAudio();
                player_animator.Play("WalkRight");
                IsWolk = true;
            }
            else if (horizontal < 0)
            {
                WalkAudio();
                player_animator.Play("WalkLeft");
                IsWolk = true;
            }
            else
            {
                WalkTime = 0;
                WalkBool = false;
                PlayerAudioSource.Stop();
                player_animator.Play("Idle");
                first = true;
                IsWolk = false;
                runBool = false;
                WolkBool = false;
            }
            if (fatigueInt > 0)
            {
                GetComponent<Rigidbody>().AddForce(transform.up * jump, ForceMode.Impulse);
            }

        }
        transform.Translate(new Vector3(horizontal, 0, vertical));
    }
    public void WalkAudio()
    {
        runBool = false;
        WolkBool = false;
        first = true;
        WalkBool = true;
        if (WalkTime >= 0.45)
        {
            WalkTime = 0;
            PlayerAudioSource.PlayOneShot(walk);
        }
    }
    public void RunAudio()
    {
        runBool = true;
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            WalkTime = 2;
        }
        if (first == true)
        {
            first = false;
            WalkTime = 2;
        }
        WalkBool = true;
        if (WalkTime >= 2)
        {
            WalkTime = 0;
            PlayerAudioSource.PlayOneShot(run);
        }
    }
    public void JumpAudio()
    {
        WalkBool = true;
        if (firstjump == true)
        {
            PlayerAudioSource.PlayOneShot(jump_audio);
            WalkTime = 0;
        }
        if (WalkTime >= 0.1)
        {
            if(fatigueInt > 0)
            {
                WalkTime = 0;
                PlayerAudioSource.PlayOneShot(jump_audio);
            }
        }
    }
    public void GetDamageGrenade()
    {
        hp -= 25;
        hp1 = hp / 50 * 100;
        hpbar.fillAmount = hp1 * 0.01f;
    }
    private void FixedUpdate()
    {
        timer_fatigue += Time.deltaTime;
        hp1 = hp / 50 * 100;
        hpbar.fillAmount = hp1 * 0.01f;
        hpText.text = Mathf.RoundToInt(hp / 50 * 100).ToString() + "%";
        if (IsWolk == false)
        {
            if (waterInt >= 10f && hungerInt >= 10f)
            {
                if (fatigueInt < 100)
                {
                    if (timer_fatigue > 0.1)
                    {
                        fatigueInt += 0.5f;
                        waterInt -= 0.03f;
                        hungerInt -= 0.03f;
                        timer_fatigue = 0;
                    }
                }
            }
        }
        timer += Time.deltaTime;
        timer1 += Time.deltaTime;
        timer_water += Time.deltaTime;
        timer_hunger += Time.deltaTime;
        hpTimer += Time.deltaTime;
        if (waterInt <= 0)
        {
            if (timer > 2f)
            {
                hp -= 1f;
                timer = 0;
            }
        }
        if (hungerInt <= 0)
        {
            if (timer1 > 2f)
            {
                hp -= 1f;
                timer1 = 0;
            }
        }
        if (waterInt > 0)
        {
            if (timer_water > 0.1)
            {
                waterInt -= 0.0002f;
                timer_water = 0;
            }
        }
        if (hungerInt > 0)
        {
            if (timer_hunger > 0.1)
            {
                hungerInt -= 0.0002f;
                timer_hunger = 0;
            }
        }
        if (waterInt > 50 && hungerInt > 50)
        {
            if (hp < 50)
            {
                if (hpTimer > 0.1)
                {
                    hp += 0.01f;
                    waterInt -= 0.03f;
                    hungerInt -= 0.03f;
                    hpTimer = 0;
                }
            }
        }
        if (fatigueInt <= 0)
        {
            fatigueInt = 0;
            firstjump = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "EnemyPanch")
        {
            if (TimeEnemy > 1)
            {
                hp -= 25;
                TimeEnemy = 0;
            }
        }
        if (other.gameObject.tag == "EnemyPanch1")
        {
            if (TimeEnemy > 1)
            {
                hp -= 49f;
                TimeEnemy = 0;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "fire")
        {
            if (fireTimmer >= 0.5f)
            {
                hp -= 10;
                fireTimmer = 0;
            }
        }
    }
}